<?php

namespace App\Http\Controllers;

use App\Companys;
use Illuminate\Http\Request;
use App\Incidents;
use App\Instances;
use Mockery\Exception;
use App\Pictures;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Str;

class IncidentsWebController extends Controller
{









    public function lists(){


        return view('incidents.list');
    }


    public function getdata(){
        //si el usuario es super administrador devolvera todas las incidencias
        if(Auth::user()->hasRole(['superadministrador'])){
            $incidents = DB::select('SELECT incidents.*,instances.name as instance_name,companys.name as company_name,
                                      (select count(*) from pictures 
                                      where pictures.incident_id = incidents.id) as pictures_quantity
                                      from incidents 
                                      left join instances on incidents.instance_id = instances.id
                                      left join companys on instances.company_id = companys.id
                                      where incidents.incidenttype_id in (1,2,3)
                                      ')
            ;

            return DataTables::of($incidents)->make(true);
        }else{
            $company_id =Auth::user()->company_id;
            $company = Companys::find($company_id);

            $incidents = DB::select('SELECT incidents.*,instances.name as instance_name,companys.name as company_name,
                                      (select count(*) from pictures where pictures.incident_id = incidents.id) as pictures_quantity
                                      from incidents 
                                      left join instances on incidents.instance_id = instances.id
                                      left join companys on instances.company_id = companys.id
                                      where companys.id = '.$company_id.' 
                                      and incidents.incidenttype_id in (1,2,3)
          
              ');
            return DataTables::of($incidents)->make(true);
        }
    }


    public function details($incident_id){
        $incident = Incidents::findOrFail($incident_id);

        return view('incidents.details',compact('incident'));
    }


    public function approved($incident_id){
        $incident = Incidents::findOrFail($incident_id);

        if($incident->approved){
            $incident->approved = 0;
        }else{
            $incident->approved = 1;
        }

        //si se creo el registro el registro setea el sucess en true
        if($incident->save()){
            $sucess = true;
        }else{
            $sucess = false;
        }
        //variable donde debe dirigirse el elemento
        $returnUrl = url('/')."/main/incidents/".$incident_id;
        return view('pages.genericprocess',compact('returnUrl','sucess'));

    }


    public function generatereport($incident_id){
        $incident = Incidents::findOrFail($incident_id);

        $pdf = PDF::loadView('incidents.report', compact('incident'))->setOptions(['isRemoteEnabled' => true,'name'=>'Reporte de Incidencias']);
        return $pdf->stream("Reporte de Incidencias ID".$incident->id);

        //  return view('incidents.report',compact('incident'));

    }



    public function publicdocument($incident_id,$key){

        $incident = Incidents::where('key','=',$key)->where('incidenttype_id','!=',4)->first();

        if($incident){
            $pdf = PDF::loadView('incidents.report', compact('incident'))->setOptions(['isRemoteEnabled' => true,'name'=>'Reporte de Incidencias']);
            return $pdf->stream("Reporte de Incidencias ID".$incident->id);
        }else {
            return "Este documento no existe";
        }


    }
}
