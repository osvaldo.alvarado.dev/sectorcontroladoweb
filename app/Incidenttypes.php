<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incidenttypes extends Model
{
    protected $fillable = ['id','name','priority'];

}
