<?php

use Illuminate\Database\Seeder;
use App\Rounds;

class RoundsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $element = new Rounds(); $element->name='Ronda 1 - rubem';$element->hour_to_round = '14:00'; $element->hour_to_round_finish = '14:30'; $element->instance_id=1; $element->enabled=1; $element->save();
        $element = new Rounds(); $element->name='Ronda 2 - ruben';$element->hour_to_round = '17:00'; $element->hour_to_round_finish = '17:30'; $element->instance_id=1; $element->enabled=1;$element->save();
        $element = new Rounds(); $element->name='Ronda 3 - otro';$element->hour_to_round = '18:00'; $element->hour_to_round_finish = '18:30'; $element->instance_id=2; $element->enabled=1;$element->save();
        $element = new Rounds(); $element->name='Ronda 4 - otro';$element->hour_to_round = '19:00'; $element->hour_to_round_finish = '19:30'; $element->instance_id=2;$element->enabled=1;$element->save();

    }
}
