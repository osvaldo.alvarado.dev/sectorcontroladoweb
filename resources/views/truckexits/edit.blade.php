
@extends('containers.maincontainer')

@section('content')
    <div class="box">


        <div class="box-body">
            <h2 class="page-header">
                <i class="fa fa-pencil"></i> Generando salida de camiones
            </h2>

                <form method="post" action="{{url('main/truckexits/'.$incident->id.'/edit/process')}}">


                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="exampleInputEmail1">Hora de Entrada: </label>
                        <input required type="time" class="form-control" name="inside_hour" value="{{isset($incident->inside_hour) ? $incident->inside_hour : '' }}">
                    </div>


                    <div class="form-group">
                        <label for="exampleInputEmail1">Hora de Salida: </label>
                        <input required type="time" class="form-control" name="exit_hour" value="{{isset($incident->exit_hour) ? $incident->exit_hour : '' }}">
                    </div>


                    <div class="form-group">
                        <label for="exampleInputEmail1">Certificado de Desinfeccion: </label>
                        <input type="text" class="form-control" name="desinfection_document" value="{{isset($incident->desinfection_document) ? $incident->desinfection_document : '' }}">
                    </div>


                    <div class="form-group">
                        <label for="exampleInputEmail1">Nombre Conductor: </label>
                        <input type="text" class="form-control" name="driver" value="{{isset($incident->driver) ? $incident->driver : '' }}">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Numero Guia Despacho: </label>
                        <input required type="text" class="form-control" name="guide_number" value="{{isset($incident->guide_number) ? $incident->guide_number : '' }}">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Bins: </label>
                        <input required type="text" class="form-control" name="bins" value="{{isset($incident->bins) ? $incident->bins : '' }}">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Kilogramos: </label>
                        <input required type="text" class="form-control" name="kilograms" value="{{isset($incident->kilograms) ? $incident->kilograms : '' }}">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Origen: </label>
                        <input required type="text" class="form-control" name="origin" value="{{isset($incident->origin) ? $incident->origin : '' }}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Destino: </label>
                        <input required type="text" class="form-control" name="destination" value="{{isset($incident->destination) ? $incident->destination : '' }}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Sellos: </label>
                        <input required type="text" class="form-control" name="sails" value="{{isset($incident->sails) ? $incident->sails : '' }}">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Telefono: </label>
                        <input required type="text" class="form-control" name="phone" value="{{isset($incident->phone) ? $incident->phone : '' }}">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Comentario: </label>
                        <input required type="text" class="form-control" name="comentary" value="{{isset($incident->comentary) ? $incident->comentary : '' }}">
                    </div>

                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>Guardar</button>
                        </form>
        </div>


    </div>
@stop