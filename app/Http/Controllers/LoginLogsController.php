<?php

namespace App\Http\Controllers;

use App\LoginLogs;
use Illuminate\Http\Request;

class LoginLogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoginLogs  $loginLogs
     * @return \Illuminate\Http\Response
     */
    public function show(LoginLogs $loginLogs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoginLogs  $loginLogs
     * @return \Illuminate\Http\Response
     */
    public function edit(LoginLogs $loginLogs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoginLogs  $loginLogs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoginLogs $loginLogs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoginLogs  $loginLogs
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoginLogs $loginLogs)
    {
        //
    }
}
