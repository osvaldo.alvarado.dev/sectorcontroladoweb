<?php

namespace App\Http\Controllers;

use App\Roundcards;
use Illuminate\Http\Request;

class RoundcardsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Roundcards  $roundcards
     * @return \Illuminate\Http\Response
     */
    public function show(Roundcards $roundcards)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Roundcards  $roundcards
     * @return \Illuminate\Http\Response
     */
    public function edit(Roundcards $roundcards)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Roundcards  $roundcards
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Roundcards $roundcards)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Roundcards  $roundcards
     * @return \Illuminate\Http\Response
     */
    public function destroy(Roundcards $roundcards)
    {
        //
    }
}
