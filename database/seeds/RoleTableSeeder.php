<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = 'superadministrador';
        $role->description = 'Un Usuario Super administrador';
        $role->save();



        $role = new Role();
        $role->name = 'administradorinstancia';
        $role->description = 'Un Usuario Super administrador';
        $role->save();

    }
}
