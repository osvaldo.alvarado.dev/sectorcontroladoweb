<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pictures extends Model
{
    protected $fillable = ['id','incident_id','uri','name','description','thumb_uri','latitude','longitude'];

}
