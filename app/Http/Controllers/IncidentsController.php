<?php

namespace App\Http\Controllers;

use App\Companys;
use Illuminate\Http\Request;
use App\Incidents;
use App\Instances;
use Mockery\Exception;
use App\Pictures;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Str;

class IncidentsController extends Controller
{

    public function store(Request $request)
    {
        //obtiene la llave de la instancia
        $key = $request->header('key');

        if(!isset($key)){
            return response()->json([
                'error' => 'Debe configurar una llave'], 400);
        }else{
            if($key ==""){
                return response()->json([
                    'error' => 'Debe configurar una llave -'], 400);
            }
        }

        try{
            //busca la instancia asociada segun la KEY
            $instance = Instances::all()->where('key','=',$key)->first();

            //valida que la llave exista
            if($instance){
                $request->request->add(['instance_id'=>$instance->id]);
                $incident = Incidents::create($request->all());
                $incident->key = Str::random(20);
                $incident->save();

                if($incident){
                    return response()->json($incident, 201);
                }else{
                    return response()->json([
                        'error' => 'La incidencia no pudo ser creada en el sistema'], 400);
                }
            }else{
                return response()->json([
                    'error' => 'La llave utilizada no existe en el sistema'], 400);
            }

        }catch (Exception $e){
            return response()->json([
                'error' => $e], 400);

        }
    }

    public function index2(Request $request){
        //obtiene la llave de la instancia
        $key = $request->header('key');

        if(!isset($key)){
            return response()->json([
                'error' => 'Debe configurar una llave'], 400);
        }else{
            if($key ==""){
                return response()->json([
                    'error' => 'Debe configurar una llave -'], 400);
            }
        }

        //busca la instancia asociada segun la KEY
        $instance = Instances::all()->where('key','=',$key)->first();


        if($instance){
            //busca solo las instancias de
            $incidents = Incidents::orderBy('id','desc')->where('instance_id','=',$instance->id)->limit(100)->get();

            return response()->json($incidents,200);
        }else{
            return response()->json([
                'error' => 'La llave utilizada no existe en el sistema'], 400);
        }


    }


    public function show($incident_id){
        //obtiene el incidente mas sus fotografias
        $incidentWithPictures = Incidents::where('id','=',$incident_id)->get()->first();

        //obtiene las fotografias del incidente
        $pictures = Pictures::where('incident_id','=',$incident_id)->orderBy('id','Desc')->get();

        //incluye las fotografias en la incidencia
        $incidentWithPictures->setAttribute("pictures",$pictures);

        //retorna el objeto
        return response()->json($incidentWithPictures,200,[],JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
    }


    public function lists(){


        return view('incidents.list');
    }


    public function getdata(){
        //si el usuario es super administrador devolvera todas las incidencias
        if(Auth::user()->hasRole(['superadministrador'])){
            $incidents = DB::select('SELECT incidents.*,instances.name as instance_name,companys.name as company_name,
                                      (select count(*) from pictures 
                                      where pictures.incident_id = incidents.id) as pictures_quantity
                                      from incidents 
                                      left join instances on incidents.instance_id = instances.id
                                      left join companys on instances.company_id = companys.id
                                      where incidents.incidenttype_id in (1,2,3)
                                      ')
                                      ;

            return DataTables::of($incidents)->make(true);
        }else{
            $company_id =Auth::user()->company_id;
            $company = Companys::find($company_id);

            $incidents = DB::select('SELECT incidents.*,instances.name as instance_name,companys.name as company_name,
                                      (select count(*) from pictures where pictures.incident_id = incidents.id) as pictures_quantity
                                      from incidents 
                                      left join instances on incidents.instance_id = instances.id
                                      left join companys on instances.company_id = companys.id
                                      where companys.id = '.$company_id.' 
                                      and incidents.incidenttype_id in (1,2,3)
          
              ');
            return DataTables::of($incidents)->make(true);
        }
    }


    public function details($incident_id){
        $incident = Incidents::findOrFail($incident_id);

        return view('incidents.details',compact('incident'));
    }


    public function approved($incident_id){
        $incident = Incidents::findOrFail($incident_id);

        if($incident->approved){
            $incident->approved = 0;
        }else{
            $incident->approved = 1;
        }

        //si se creo el registro el registro setea el sucess en true
        if($incident->save()){
            $sucess = true;
        }else{
            $sucess = false;
        }
        //variable donde debe dirigirse el elemento
        $returnUrl = url('/')."/main/incidents/".$incident_id;
        return view('pages.genericprocess',compact('returnUrl','sucess'));

    }


    public function generatereport($incident_id){
        $incident = Incidents::findOrFail($incident_id);

        $pdf = PDF::loadView('incidents.report', compact('incident'))->setOptions(['isRemoteEnabled' => true,'name'=>'Reporte de Incidencias']);
        return $pdf->stream("Reporte de Incidencias ID".$incident->id);

      //  return view('incidents.report',compact('incident'));

    }


}
