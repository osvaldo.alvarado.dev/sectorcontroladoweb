
@extends('containers.maincontainer')

@section('content')

    <div class="box">
        <div class="box-body">
            <h2 class="page-header">
                <i class="fa fa-edit"></i> Incidencias:
            </h2>

            <div class="table-responsive">
                <table id="desinfections_table" class="table table-bordered table-hover table-striped dataTable">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Guardia</th>
                        <th>Titulo</th>
                        <th>Descripcion</th>
                        <th><i class="fa fa-camera"></i></th>
                        <th>Instancia</th>
                        <th>Detalles</th>
                        <th>Detalles</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/af-2.3.0/b-1.5.2/b-colvis-1.5.2/b-flash-1.5.2/b-html5-1.5.2/b-print-1.5.2/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.4.0/r-2.2.2/rg-1.0.3/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>
    <script src="{{ url('/') }}/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#desinfections_table').DataTable({
                responsive: true,
                fixedHeader: {
                    headerOffset: 0
                },



                "processing" : true,
                "serverSide" : true,

                "ajax" : "{{ url('/') }}/main/incidents/getdata",
                "columns" : [
                    {"data" : "id"},
                    {"data" : "guard"},
                    {"data" : "title"},
                    {"data" : "description"},
                    {"data" : "pictures_quantity"},
                    {
                        "render": function ( data, type, full, meta ) {
                            var returnString = full.instance_name + " <small>" + full.company_name + "</small>";
                            return returnString;
                        }
                    },

                    {
                        "render": function ( data, type, full, meta ) {

                            returnString = "";
                            if(full.approved == 0){
                                returnString ="<span class=\"badge bg-red\"><i class='fa fa-close'></i> Pendiente</span>";
                            }else{
                                returnString ="<span class=\"badge bg-green\"><i class='fa fa-check'></i> Aprobado</span>";
                            }

                            return returnString;
                        }
                    },
                    {
                        "render": function ( data, type, full, meta ) {

                            var returnString = "<div class=\"btn-group\"> " +
                                " <button type=\"button\" class=\"btn btn-success dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">Opciones " +
                                "<span class=\"caret\"></span><span class=\"sr-only\">Toggle Dropdown</span></button>";

                            returnString = returnString + "  <ul class=\"dropdown-menu dropdown-menu-right primary\" role=\"menu\">";
                            returnString = returnString + "<li><a href='{{ url('/')}}/main/users/"+full.id+"'> <i class='fa fa-search'></i>Detalle</a></li>";

                            returnString = returnString + "</ul></div>";

                            returnString = "<a class='btn btn-success' href='{{ url('/')}}/main/incidents/"+full.id+"'> <i class='fa fa-search'></i>Detalle</a>";

                            return returnString;
                        }
                    },

                ],
                language: {
                    "lengthMenu": "Mostrar _MENU_ registros por pagina &nbsp;&nbsp;&nbsp;",
                    "zeroRecords": "No se encuentra ningun registro",
                    "info": "Pagina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros",
                    "infoFiltered": "(buscando entre _MAX_ registros)",
                    "search":         "Filtrar Registros : &nbsp",
                    "processing" : "Cargando...",
                    paginate: {
                        first:      "Primera Pagina",
                        previous:   "Anterior",
                        next:       "Siguiente",
                        last:       "Ultima"
                    },

                },
                "order": [[ 0, "desc" ]]
            })
        });

    </script>

@stop