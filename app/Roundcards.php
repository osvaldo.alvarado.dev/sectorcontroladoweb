<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roundcards extends Model
{
    protected $fillable = ['card_id','round_id'];
    public function card(){
        return $this->hasOne('App\Cards','id','card_id');
    }
}
