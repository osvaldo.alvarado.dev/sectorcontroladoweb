@if(isset(Auth::user()->email))

@else

    <script>window.location="/main"</script>
@endif

<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>App</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ url('/') }}/bower_components/bootstrap/dist/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/af-2.3.0/b-1.5.2/b-colvis-1.5.2/b-flash-1.5.2/b-html5-1.5.2/b-print-1.5.2/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.4.0/r-2.2.2/rg-1.0.3/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.css"/>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ url('/') }}/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ url('/') }}/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('/') }}/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="{{ url('/') }}/dist/css/skins/skin-red.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery 3 -->
    <script src="{{ url('/') }}/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap 3.3.7 -->
    <script src="{{ url('/') }}/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{ url('/') }}/dist/js/adminlte.min.js"></script>
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">


</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">
    <!-- Main Header -->
    <header class="main-header">

        <!-- Logo -->
        <a href="index2.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>App</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b></b>App</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">

            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- Sidebar user panel (optional) -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ url('/') }}/dist/img/user.png" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

            <!-- search form (Optional) -->

            <!-- /.search form -->

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">Menu Principal</li>
                <!-- Optionally, you can add icons to the links -->

                <li {!! classActivePath('main/home') !!}><a href="{{ url('/') }}/main/home"><i class="fa fa-home"></i> <span>Inicio</span></a></li>
                @if(Auth::user()->hasRole(['administrador','administrativo','empleado']))
                    <li {!! classActivePath('main/desinfeccion') !!}><a href="{{ url('/') }}/main/desinfeccion/"><i class="fa fa-list"></i> <span>Listado Informes</span></a></li>
                @endif

                @if(Auth::user()->hasRole(['administrador','administrativo','empleado']))
                    <li {!! classActivePath('main/desinfeccion/addregister') !!}><a href="{{ url('/') }}/main/desinfeccion/addregister"><i class="fa fa-pencil"></i> <span>Crear Informe</span></a></li>
                @endif

                @if(Auth::user()->hasRole(['administrador','administrativo']))
                    <li {!! classActivePath('main/areas') !!}><a href="{{ url('/') }}/main/areas/"><i class="fa fa-plus-circle"></i> <span>Areas</span></a></li>
                @endif

                @if(Auth::user()->hasRole(['administrador']))
                    <li {!! classActivePath('main/users') !!}><a href="{{ url('/') }}/main/users/"><i class="fa fa-user-circle-o"></i> <span>Usuarios</span></a></li>
                @endif

                @if(Auth::user()->hasRole(['administrador','administrativo']))
                    <li {!! classActivePath('main/clients') !!}><a href="{{ url('/') }}/main/clients"><i class="fa fa-users"></i> <span>Clientes</span></a></li>
                @endif

                @if(Auth::user()->hasRole(['administrador','administrativo']))
                    <li {!! classActivePath('main/products') !!}><a href="{{ url('/') }}/main/products"><i class="fa fa-clipboard"></i> <span>Productos</span></a></li>
                @endif


                @if(Auth::user()->hasRole(['superadministrador','administradorinstancia']))
                    <li {!! classActivePath('main/marks') !!}><a href="{{ url('/') }}/main/marks"><i class="fa fa-calendar-check-o"></i> <span>Reporte por Fecha</span></a></li>
                @endif


            @if(Auth::user()->hasRole(['superadministrador','administradorinstancia']))
                    <li {!! classActivePath('main/marksByDay') !!}><a href="{{ url('/') }}/main/marksByDay"><i class="fa fa-check"></i> <span>Controles Semanales</span></a></li>
                @endif
                @if(Auth::user()->hasRole(['superadministrador','administradorinstancia']))
                    <li {!! classActivePath('main/incidents') !!}><a href="{{ url('/') }}/main/incidents"><i class="fa fa-edit"></i> <span>Incidencias</span></a></li>
                @endif

                @if(Auth::user()->hasRole(['superadministrador','administradorinstancia']))
                    <li {!! classActivePath('main/instances') !!}><a href="{{ url('/') }}/main/instances"><i class="fa fa-star"></i> <span>Instancias</span></a></li>
                @endif
                @if(Auth::user()->hasRole(['superadministrador','administradorinstancia','supervisor']))
                    <li {!! classActivePath('main/truckexits') !!}><a href="{{ url('/') }}/main/truckexits"><i class="fa fa-truck"></i> <span>Salidas Camiones</span></a></li>
                @endif



                @if(Auth::user()->hasRole(['superadministrador','administradorinstancia']))
                    <li {!! classActivePath('main/users') !!}><a href="{{ url('/') }}/main/users"><i class="fa fa-group"></i> <span>Usuarios</span></a></li>
                @endif

                @if(Auth::user()->hasRole(['superadministrador']))
                    <li {!! classActivePath('main/companys') !!}><a href="{{ url('/') }}/main/companys"><i class="fa fa-industry"></i> <span>Compañias</span></a></li>
                @endif

                @if(Auth::user()->hasRole(['administrador','administrativo']))
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-pie-chart"></i>
                            <span>Reportes</span>
                            <span class="pull-right-container">
                         <i class="fa fa-angle-left pull-right"></i>
                         </span>
                        </a>
                        <ul class="treeview-menu" style="display: none;">
                            <li  {!! classActivePath('main/reports/desinfectionsByDate') !!} ><a href="{{ url('/') }}/main/reports/desinfectionsByDate"><i class="fa fa-circle-o"></i>Informes por Cliente</a></li>
                            <li  {!! classActivePath('main/reports/desinfectionsByUser') !!} ><a href="{{ url('/') }}/main/reports/desinfectionsByUser"><i class="fa fa-circle-o"></i>Informes por Usuario</a></li>
                            <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i>Otro Reporte</a></li>
                            <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i>Otro Reporte B</a></li>


                        </ul>
                    </li>
                @endif


                @if(Auth::user()->hasRole(['administrador','administrativo']))
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-pie-chart"></i>
                            <span>Reportes</span>
                            <span class="pull-right-container">
                         <i class="fa fa-angle-left pull-right"></i>
                         </span>
                        </a>
                        <ul class="treeview-menu" style="display: none;">
                            <li  {!! classActivePath('main/reports/desinfectionsByDate') !!} ><a href="{{ url('/') }}/main/reports/desinfectionsByDate"><i class="fa fa-circle-o"></i>Informes por Cliente</a></li>
                            <li  {!! classActivePath('main/reports/desinfectionsByUser') !!} ><a href="{{ url('/') }}/main/reports/desinfectionsByUser"><i class="fa fa-circle-o"></i>Informes por Usuario</a></li>
                            <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i>Otro Reporte</a></li>
                            <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i>Otro Reporte B</a></li>


                        </ul>
                    </li>
                @endif
                <li><a href="{{ url('/') }}/main/logout"><i class="fa fa-close"></i> <span>Cerrar Sesion</span></a></li>

            </ul>
            <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->


        <!-- Main content -->
        <section class="content container-fluid">

            <!--------------------------
              | Your Page Content Here |
              -------------------------->
            @yield('content')

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->


    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane active" id="control-sidebar-home-tab">
                <h3 class="control-sidebar-heading">Recent Activity</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:;">
                            <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                            <div class="menu-info">
                                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                <p>Will be 23 on April 24th</p>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

                <h3 class="control-sidebar-heading">Tasks Progress</h3>
                <ul class="control-sidebar-menu">
                    <li>
                        <a href="javascript:;">
                            <h4 class="control-sidebar-subheading">
                                Custom Template Design
                                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
                            </h4>

                            <div class="progress progress-xxs">
                                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- /.control-sidebar-menu -->

            </div>
            <!-- /.tab-pane -->
            <!-- Stats tab content -->
            <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
            <!-- /.tab-pane -->
            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
                <form method="post">
                    <h3 class="control-sidebar-heading">General Settings</h3>

                    <div class="form-group">
                        <label class="control-sidebar-subheading">
                            Report panel usage
                            <input type="checkbox" class="pull-right" checked>
                        </label>

                        <p>
                            Some information about this general settings option
                        </p>
                    </div>
                    <!-- /.form-group -->
                </form>
            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->



<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->


</body>
</html>