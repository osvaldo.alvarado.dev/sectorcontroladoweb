<?php

use Illuminate\Database\Seeder;
use App\Roundcards;

class RoundcardsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ronda 1
        $element = new Roundcards(); $element->card_id=1; $element->round_id=1; $element->save();
        $element = new Roundcards(); $element->card_id=2; $element->round_id=1; $element->save();
        $element = new Roundcards(); $element->card_id=3; $element->round_id=1; $element->save();

        //ronda2
        $element = new Roundcards(); $element->card_id=1; $element->round_id=2; $element->save();
        $element = new Roundcards(); $element->card_id=2; $element->round_id=2; $element->save();



    }
}
