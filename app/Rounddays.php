<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rounddays extends Model
{
    public function roundday(){
        return $this->hasOne('App\Rounds','id','round_id');
    }
}
