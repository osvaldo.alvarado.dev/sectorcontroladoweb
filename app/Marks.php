<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marks extends Model
{
    protected $fillable = ['card_id','latitude,longitude'];



    public function card(){
        return $this->hasOne('App\Cards','id','card_id');
    }


    public function roundday(){
        return $this->hasOne('App\Rounddays','id','roundday_id');
    }


}
