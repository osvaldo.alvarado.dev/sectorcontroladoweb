
@extends('containers.maincontainer')
<style>


    @page {
        size: auto;  /* auto is the initial value */
        margin: 0mm; /* this affects the margin in the printer settings */
    }
    @media print {
        #printPageButton {
            display: none;
        }

        img {
            height: 35%;
            max-height: 35%;
            border-style:solid;
            border-radius: 10px;
        }
    }
    @media all {
        div.saltopagina{
            display: none;
        }
    }

    @media print{
        div.saltopagina{
            display:block;
            page-break-before:always;
        }
    }

    #background{
        position:absolute;
        z-index:-10;
        background:white;
        display:block;
        min-height:50%;
        min-width:50%;
        color:yellow;
    }


</style>

@section('content')
    <div class="box box-primary">


        <div class="box-body">
            <h2 class="page-header">
                <i class="fa fa-pencil"></i> Informe de novedades:
            </h2>

            @if(!$incident->approved)
                <div class="pad margin">
                    <div class="callout callout-danger" style="margin-bottom: 0!important;">
                        <h4><i class="fa fa-warning"></i> Informe no Aprobado:</h4>
                        Debe finalizar primero este informe.
                    </div>
                </div>
            @endif

                    <address>
                        <strong>Folio: </strong> {{$incident->id}}<br>
                        <strong>Fecha: </strong> {{date('d/m/Y', strtotime($incident->created_at))}}<br>
                        <strong>Guardia: </strong> {{$incident->guard}}<br>
                        <strong>Prioridad:  </strong>
                        @switch($incident->incidenttype_id)
                            @case(1)
                            Alta
                            @break

                            @case(2)
                            Media
                            @break

                            @case(3)
                            Baja
                            @break

                        @endswitch
                        <br>
                        <strong>Titulo: </strong> {{$incident->title}}<br>
                        <strong>Descripcion: </strong> {{$incident->description}}<br>


                    </address>


            <div class="row no-print">
                <div class="col-xs-12">
                <a class="btn btn-primary" href="{{url('/main/incidents/'.$incident->id.'/generatereport')}}"><span class="fa fa-print"></span> Generar Reporte</a>
                @if(!$incident->approved)
                <a class='btn btn-success' id="printPageButton" href="{{url('/main/incidents/'.$incident->id.'/approve')}}"><i class="fa fa-check"></i> Aprobar Informe</a>
                @endif
                </div>
            </div>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-body">
            <h2 class="page-header">
                <i class="fa fa-pencil"></i> Fotografias:
            </h2>
            <?php $ct = 0?>
            @foreach($incident->pictures as $picture)
                <?php $ct = $ct + 1;?>
                {{--Verifica que sea multiplo de dos para aplicar salto de pagina--}}


                            <div>
                            <b id="printPageButton">Id Fotografia: {{$picture->id}} - </b> <br/>
                            Descripción: <b>{{$picture->description}}</b><br/>
                            </div>
                            <div>
                            <img style="width: 90%" src="{{$picture->uri}}"><br/>
                            </div>
                            <hr/>
                            <br/><br/>

            @endforeach

        </div>
    </div>





@stop