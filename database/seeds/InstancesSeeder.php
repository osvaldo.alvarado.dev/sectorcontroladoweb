<?php

use Illuminate\Database\Seeder;
use App\Instances;

class InstancesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $element = new Instances(); $element->name='Supermercado de Pruebas 1 - Testing'; $element->company_id=1; $element->save();
        $element = new Instances(); $element->name='Supermercado de Pruebas 2'; $element->company_id=1; $element->save();
        $element = new Instances(); $element->name='Cesfam'; $element->company_id=2; $element->save();
        $element = new Instances(); $element->name='Muelle'; $element->company_id=2; $element->save();
    }
}
