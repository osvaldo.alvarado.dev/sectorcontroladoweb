
@extends('containers.maincontainer')

@section('content')
    <div class="box">


        <div class="box-body">
            <h2 class="page-header">
                <i class="fa fa-pencil"></i> Detalles de la Empresa: <b>{{$company->name}}</b>
            </h2>
            <span>
            Nombre: <b>{{$company->name}}</b><br/>
            Instancias Totales: <b>{{$company->instances->count()}}</b> <br/>
            </span>
            <br/><br/>

            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <i class="fa fa-globe"></i> Instancias Actuales:
                    </h2>
                </div>
                <!-- /.col -->
            </div>

            <div class="table-responsive">
                <table class="table">
                    <tbody><tr>
                        <th style="width:50%">Nombre Instancia</th>
                        <th>Puntos de Control</th>
                        <th>Rondas</th>
                    </tr>




                    @foreach($company->instances as $instance)
                        <tr>
                            <td>{{$instance->name}}</td>
                            <td>{{$instance->cards->count()}}</td>
                            <td>{{$instance->rounds->count()}}</td>
                        </tr>

                    @endforeach
                    </tbody></table>
            </div>
        </div>
    </div>

@stop