<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use App\User;
use App\Role;
use App\Instances;
use App\Companys;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    function lists(){
        //valida que el usuario sea un administrador
        Auth::user()->authorizeRoles(['superadministrador','administradorinstancia']);
        return view('users.list');
    }

    function add(){
        //valida que el usuario sea un administrador
        Auth::user()->authorizeRoles(['superadministrador','administradorinstancia']);

        //si no es super administrador debe ser administrador de instancia
        if(Auth::user()->hasRole('superadministrador')){
            $roles = Role::all();
            $companys = Companys::all();
        }else{
            $roles = Role::whereIn('id',[2,3,4,5,6,7])->get();
            $companys = Companys::where('id','=',Auth::user()->company_id)->get();
        }

        return view('users.add',compact('roles','companys'));
    }


    function getData(){
        //valida que el usuario sea un administrador
        Auth::user()->authorizeRoles(['superadministrador','administradorinstancia']);


        //si el usuario es super administrador trae todos los usuarios
        if(Auth::user()->hasRole('superadministrador')){
            $users = DB::table('users')
                ->select('users.*');
        }else{
            //si no es super administrador debe ser administrador de instancia
            $users = DB::table('users')
                ->select('users.*')
                ->where('users.company_id','=',Auth::user()->company_id);
        }


        if( isset(Auth::user()->email)){
            return DataTables::of($users)->make(true);
        }
    }

    public function create(Request $request)
    {
        //valida que el usuario sea un administrador
        Auth::user()->authorizeRoles(['superadministrador','administradorinstancia']);

        $input = $request->all();
        //encripta la clave
        $input['password'] = bcrypt($input['password']);


        $user = User::create($input);
        //define el estado del usuario en activo

        //borra todos los permisos antiguos del usuario
        DB::table('role_user')->where('user_id', $user->id)->delete();
        $input = $request->all();
        $roles = $user->roles()->attach($input['roles']);

        //si se creo el registro el registro setea el sucess en true
        if($user){
            $sucess = true;
        }else{
            $sucess = false;
        }
        //variable donde debe dirigirse el elemento
        $returnUrl = url('/')."/main/users/";
        return view('pages.genericprocess',compact('returnUrl','sucess'));
    }


    function detail($user_id){
        //valida que el usuario sea un administrador
        Auth::user()->authorizeRoles(['superadministrador','administradorinstancia']);



        //si no es super administrador debe ser administrador de instancia
        if(Auth::user()->hasRole('superadministrador')){
            $roles = Role::all();
            $companys = Companys::all();
        }else{
            $roles = Role::whereIn('id',[2,3,4,5,6,7])->get();
            $companys = Companys::where('id','=',Auth::user()->company_id)->get();
        }



        $user = User::find($user_id);
        return view('users.detail',compact('user','roles','companys'));
    }

    function addroles($user_id,Request $request){
        //valida que el usuario sea un administrador
        Auth::user()->authorizeRoles(['superadministrador','administradorinstancia']);

        $user = User::find($user_id);

        //borra todos los permisos antiguos del usuario
        DB::table('role_user')->where('user_id', $user_id)->delete();

        $input = $request->all();
        $roles = $user->roles()->attach($input['roles']);

        //si se creo el registro el registro setea el sucess en true
        $sucess = true;
        //variable donde debe dirigirse el elemento
        $returnUrl = url('/')."/main/users/".$user_id;
        return view('pages.genericprocess',compact('returnUrl','sucess'));
    }


    function changepassword($user_id,Request $request){

        $returnUrl = url('/')."/main/users/".$user_id;
        $user = User::find($user_id);
        $input = $request->all();
        $user->password = bcrypt($input['password']);
        if($user->save()){
            $sucess = true;

        }else{
            $sucess = false;
        }
        return view('pages.genericprocess',compact('returnUrl','sucess'));

    }


    function editprocess($user_id,Request $request){

        $user = User::find($user_id);
        $input = $request->all();

        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->company_id = $input['company_id'];
        if(isset($input['status'])){
            $user->status = 1;
        }else{
            $user->status =0;
        }
        $returnUrl = url('/')."/main/users/".$user_id;
        if($user->save()){
            $sucess = true;

        }else{
            $sucess = false;
        }

        $message = "El usuario ".$user->name." se actualizo correctamente";
        return view('pages.genericprocess',compact('sucess','message','returnUrl'));

    }
}
