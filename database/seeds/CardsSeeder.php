<?php

use Illuminate\Database\Seeder;
use App\Cards;


class CardsSeeder extends Seeder
{
    /**
* Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $card = new Cards(); $card->name='62FEA281';$card->title='Sector de Testing' ; $card->instance_id = 1;$card->save();
        $card = new Cards(); $card->name='5B855801';$card->title='Punto de Control 2' ; $card->instance_id = 1; $card->save();
        $card = new Cards(); $card->name='8B036001';$card->title='Sector 6' ;$card->instance_id = 2; $card->save();
        $card = new Cards(); $card->name='5B405AXX';$card->title='Area de pruebas 8' ;$card->instance_id = 2; $card->save();


        $card = new Cards(); $card->name='5B405A01';$card->title='Primer Piso' ;$card->instance_id = 3; $card->save();
        $card = new Cards(); $card->name='9B7C5C01';$card->title='Casino' ;$card->instance_id = 3; $card->save();
        $card = new Cards(); $card->name='5B405A22';$card->title='Segundo Piso A' ;$card->instance_id = 3; $card->save();
        $card = new Cards(); $card->name='5B405A23';$card->title='Tercer Piso B' ;$card->instance_id = 3; $card->save();
        $card = new Cards(); $card->name='5B405A24';$card->title='Tercer Piso C' ;$card->instance_id = 3; $card->save();
        $card = new Cards(); $card->name='5B405A25';$card->title='Entrada' ;$card->instance_id = 4; $card->save();
        $card = new Cards(); $card->name='5B405A39';$card->title='Oficina 1' ;$card->instance_id = 4; $card->save();

    }
}
