
@extends('containers.maincontainer')

@section('content')
    <div class="box">


        <div class="box-body">
            <h2 class="page-header">
                <i class="fa fa-pencil"></i> Creando Nueva Instancia
            </h2>
            @if(isset($instance))
                <form method="post" action="{{url('main/instances/'.$instance->id.'/edit/process')}}">
            @else
                <form method="post" action="{{url('main/instances/add/process')}}">
            @endif

            {{csrf_field()}}


                <div class="form-group">
                    <label for="exampleInputEmail1">Nombre de la Instancia: </label>
                    <input required type="text" class="form-control" name="name" value="{{isset($instance->name) ? $instance->name : '' }}" required>
                </div>


                <div class="form-group">
                    <label for="exampleInputEmail1">Compañia: </label>
                    <select required class="form-control" name="company_id" required>
                        <option value="">--seleccione un elemento---</option>
                        @foreach($companys as $company)
                            @if (old('company_id') == $company->id)
                                <option selected value="{{$company->id}}"> {{$company->name}} </option>
                            @else
                                <option {{isset($instance->company_id) ? $instance->company_id == $company->id ? 'selected' : '' : '' }} value="{{$company->id}}"> {{$company->name}} </option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Direccion: </label>
                    <input required type="text" class="form-control" name="address" value="{{isset($instance->address) ? $instance->address : '' }}" required>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Latitude: </label>
                    <input  type="text" class="form-control" name="lat" value="{{isset($instance->lat) ? $instance->lat : '' }}">
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Longitude: </label>
                    <input type="text" class="form-control" name="long" value="{{isset($instance->long) ? $instance->long : '' }}">
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Llave: </label>
                    <input type="text" class="form-control" name="key" value="{{isset($instance->key) ? $instance->key : '' }}" maxlength="8">
                </div>
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>Guardar</button>
            </form>
        </div>


    </div>
@stop