<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */


    public $variable;
    public $selected_rounds_to_alert;
    public $controled_sectors;

    public function __construct(String $variable,Collection $selected_rounds_to_alert,Collection $controled_sectors)
    {
        $this->variable = $variable;
        $this->subject = "Alerta en: ".$variable." Sectores no Controlados ".date('d-m-Y H:i:s');
        $this->selected_rounds_to_alert = $selected_rounds_to_alert;
        $this->controled_sectors = $controled_sectors;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.SectorQuantities');
    }
}
