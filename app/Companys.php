<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companys extends Model
{
    protected $fillable = ['id','name'];


    public function instances(){
        return $this->hasMany('App\Instances','company_id','id');
    }

}
