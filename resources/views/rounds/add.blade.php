
@extends('containers.maincontainer')

@section('content')
    <div class="box">


        <div class="box-body">
            <h2 class="page-header">
                <i class="fa fa-pencil"></i> Crear Ronda
            </h2>

            @if ($message = Session::get('error'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i>No se puede guardar</h4>
                    <p>{{$message}}</p>
                </div>
            @endif

            @if(isset($round))
                <form method="post" action="{{url('/main/rounds/'.$round->id.'/edit/process')}}">
            @else
                <form method="post" action="{{url('/main/instances/'.$instance->id.'/addround/process')}}">
            @endif

                {{csrf_field()}}


                <div class="form-group">
                    <label for="exampleInputEmail1">Nombre de la Ronda </label>
                    <input required type="decimal" class="form-control" name="name" value="{{isset($round->name) ? $round->name : old('name','') }}" required>
                </div>


                <div class="form-group">
                    <label for="exampleInputEmail1">Desde: </label>
                    <input required type="time" class="form-control" name="hour_to_round" value="{{isset($round->hour_to_round) ? $round->hour_to_round : old('hour_to_round','') }}" required>
                </div>


                <div class="form-group">
                    <label for="exampleInputEmail1">Hasta: </label>
                    <input required type="time" class="form-control" name="hour_to_round_finish" value="{{isset($round->hour_to_round_finish) ? $round->hour_to_round_finish : old('hour_to_round_finish','') }}" required>
                </div>



                <div class="form-group">
                    <label for="exampleInputEmail1">Tarjetas: </label>
                    <div class="form-group">

                        <label for="exampleInputEmail1"></label>
                        @foreach($cards as $card)
                            <div class="form-check">

                                @if(isset($round))
                                    {{--Consulta si la tarjeta iterada esta contenida en la ronda (si es edicion) --}}

                                    @if($round->hasCard($card->id,$round->id))
                                        <input class="form-check-input" checked="true" type="checkbox" value="{{$card->id}}" name="cards[]">
                                    @else
                                        <input class="form-check-input"  type="checkbox" value="{{$card->id}}" name="cards[]">
                                    @endif

                                @else
                                    <input class="form-check-input" type="checkbox" value="{{$card->id}}" name="cards[]">
                                @endif
                                <label class="form-check-label" for="defaultCheck1">
                                    {{$card->title}}
                                </label>
                            </div>
                        @endforeach
                </div>


                    <div class="form-group">
                        <label for="exampleInputEmail1">Dias de la semana: </label>
                        <div class="form-group">
                            <label for="exampleInputEmail1"></label>
                            @foreach($days as $day)
                                <div class="form-check">
                                    @if(isset($round))
                                        {{--Consulta si la tarjeta iterada esta contenida en la ronda (si es edicion) --}}
                                        @if($round->hasDay($day,$round->id))
                                            <input class="form-check-input" checked="true" type="checkbox" value="{{$day}}" name="days[]">
                                        @else
                                            <input class="form-check-input"  type="checkbox" value="{{$day}}" name="days[]">
                                        @endif

                                    @else
                                        <input class="form-check-input" type="checkbox" value="{{$day}}" name="days[]">
                                    @endif
                                    <label class="form-check-label" for="defaultCheck1">
                                        {{$days_of_week[$day]}}
                                    </label>
                                </div>
                            @endforeach
                        </div>


                        <div class="form-group">
                            <label for="exampleInputEmail1">Activa</label>
                            <select required class="form-control" name="enabled" id="enabled">
                                <option {{isset($round->enabled) ? $round->enabled == 1 ? 'selected' : '' : '' }} value="1">Activa</option>
                                <option {{isset($round->enabled) ? $round->enabled == 0 ? 'selected' : '' : '' }} value="0">Desactivada</option>
                            </select>
                        </div>


                        <div class="form-group">
                            <label for="exampleInputEmail1">Alertas por Correos</label>
                            <select required class="form-control" name="enable_alert" id="enable_alert">
                                <option {{isset($round->enable_alert) ? $round->enable_alert == 1 ? 'selected' : '' : '' }} value="1">Habilitado</option>
                                <option {{isset($round->enable_alert) ? $round->enable_alert == 0 ? 'selected' : '' : '' }} value="0">Deshabilitar</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Mails para alertar (Ingrese correos separados por ; para mas de uno) : </label>
                            <input type="text" class="form-control" name="mails_to_alert" value="{{isset($round->mails_to_alert) ? $round->mails_to_alert : old('mails_to_alert','') }}">
                        </div>




                        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>Guardar</button>
            </form>


        </div>


    </div>
@stop