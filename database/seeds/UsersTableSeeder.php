<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $permisos = Role::where('name', 'superadministrador')->first();


       $employee = new User();
       $employee->name = 'Osvaldo Alvarado';
       $employee->email = 'osvaldo.alvarado.dev@gmail.com';
       $employee->password = bcrypt('password');
       $employee->status = 1;
       $employee->save();
       $employee->roles()->attach($permisos);


        $permisos = Role::where('name', 'administradorinstancia')->first();
        $employee = new User();
        $employee->name = 'Ruben';
        $employee->email = 'ruben@gmail.com';
        $employee->password = bcrypt('password');
        $employee->status = 1;
        $employee->company_id = 2;
        $employee->save();
        $employee->roles()->attach($permisos);






    }


}
