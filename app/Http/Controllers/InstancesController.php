<?php

namespace App\Http\Controllers;

use App\Companys;
use App\Instances;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;
use App\Cards;

class InstancesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function list()
    {

        return view('instances.list');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Instances  $instances
     * @return \Illuminate\Http\Response
     */
    public function show(Instances $instances)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Instances  $instances
     * @return \Illuminate\Http\Response
     */
    public function edit(Instances $instances)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Instances  $instances
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Instances $instances)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Instances  $instances
     * @return \Illuminate\Http\Response
     */
    public function destroy(Instances $instances)
    {
        //
    }



    public function getAjaxInstances(){
        //valida que el usuario tenga la sesion iniciada
        if(Auth::user()){

            //valida que el usuario sea super administrador
            $is_super_administrator = Auth::user()->hasRole(['superadministrador']);

            //si el usuario es super administrador devuelve todas las instancias
            if($is_super_administrator){
                $instances = Instances::all();
                return DataTables::of($instances)->make(true);
            }else{
                //obtiene la compañia del usuario si es que existe
                $company_user_id = Auth::user()->company_id;
                $instances = Instances::where('company_id','=',$company_user_id);
                return DataTables::of($instances)->make(true);
            }

        }
        else{
            return "";
        }
    }


    public function add()
    {
        $companys = Companys::all();

        return view('instances.add',compact('companys'));
    }


    public function addprocess(Request $request)
    {
        $input = $request->all();


        $instance = Instances::create($input);

        $returnUrl = url('/')."/main/instances";
        $sucess = true;
        return view('pages.genericprocess',compact('returnUrl','sucess'));
    }


    public function details($instance_id){

        $instance = Instances::find($instance_id);

        $days_of_week = ['','DO','LU','MA','MI','JU','VI','SA'];

        return view('instances.details',compact('instance','days_of_week'));


    }



    public function addcard($instance_id){

        $instance = Instances::find($instance_id);

        return view('cards.add',compact('instance'));
    }



    public function addcardprocess($instance_id,Request $request)
    {
        $instance = Instances::find($instance_id);
        $input = $request->all();


        $card = Cards::create($input);

        $returnUrl = url('/')."/main/instances/".$instance->id;
        $sucess = true;
        return view('pages.genericprocess',compact('returnUrl','sucess'));
    }


    public function editinstance($instance_id){
        $instance = Instances::findOrFail($instance_id);
        $companys = Companys::all();
        return view('instances.add',compact('instance','companys'));
    }

    public function editinstanceprocess($instance_id,Request $request){
        $instance = Instances::findOrFail($instance_id);
        $input = $request->all();
        $instance->update($input);
        $instance->save();
        $returnUrl = url('/')."/main/instances/".$instance->id;
        $sucess = true;
        return view('pages.genericprocess',compact('returnUrl','sucess'));
    }
}
