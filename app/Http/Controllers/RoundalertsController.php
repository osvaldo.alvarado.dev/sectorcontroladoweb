<?php

namespace App\Http\Controllers;

use App\Roundalerts;
use Illuminate\Http\Request;

class RoundalertsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Roundalerts  $roundalerts
     * @return \Illuminate\Http\Response
     */
    public function show(Roundalerts $roundalerts)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Roundalerts  $roundalerts
     * @return \Illuminate\Http\Response
     */
    public function edit(Roundalerts $roundalerts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Roundalerts  $roundalerts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Roundalerts $roundalerts)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Roundalerts  $roundalerts
     * @return \Illuminate\Http\Response
     */
    public function destroy(Roundalerts $roundalerts)
    {
        //
    }
}
