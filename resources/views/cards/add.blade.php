
@extends('containers.maincontainer')

@section('content')
    <div class="box">


        <div class="box-body">
            <h2 class="page-header">
                <i class="fa fa-pencil"></i>Cargando Tarjeta en Instancia
            </h2>

            <form method="post" action="{{url('/main/instances/'.$instance->id.'/addcard/process')}}">
                {{csrf_field()}}

                <input id="instance_id" name="instance_id" type="hidden" value="{{$instance->id}}">
                <div class="form-group">
                    <label for="exampleInputEmail1">Numero de Serie de la Tarjeta: </label>
                    <input required type="decimal" class="form-control" name="name" value="{{isset($card->name) ? $card->name : '' }}" required>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Area donde se instalara la tarjeta </label>
                    <input required type="decimal" class="form-control" name="title" value="{{isset($card->title) ? $card->title : '' }}" required>
                </div>


                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>Guardar</button>
            </form>
        </div>


    </div>
@stop