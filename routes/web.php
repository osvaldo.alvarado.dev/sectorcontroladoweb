<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.login');
})->name('login');



route::Post('/main/checklogin','MainController@checklogin');

Route::get('/main/hello','MainController@hello');

Route::group(['middleware' => ['auth']], function() {

    Route::get('/test', function () {
        return view('pages.login');
    });


    Route::get('/main/home','MainController@home');
    route::get('/main/logout','MainController@logout');

//rutas principales
    route::get('/main/marks/bydateandround/{date_string}/{round_id}/{day_number}','MarksController@getByDateAndRound');
    route::get('/main/marks/','MarksController@items');
    route::get('/main/marksByDay','MarksController@itemsByDay');
    route::get('/main/instances/','InstancesController@list');


    route::get('/main/ajax/getinstances','InstancesController@getAjaxInstances');


    route::get('/main/instances/add','InstancesController@add');
    route::post('/main/instances/add/process','InstancesController@addProcess');
    route::get('/main/instances/{instance_id}','InstancesController@details');

    route::get('/main/instances/{instance_id}/addcard','InstancesController@addcard');
    route::post('/main/instances/{instance_id}/addcard/process','InstancesController@addcardprocess');
    route::get('/main/instances/{instance_id}/edit','InstancesController@editinstance');
    route::post('/main/instances/{instance_id}/edit/process','InstancesController@editinstanceprocess');


    route::get('/main/instances/{instance_id}/addround','RoundsController@add');
    route::post('/main/instances/{instance_id}/addround/process','RoundsController@addProcess');
    route::get('/main/rounds/{round_id}/edit','RoundsController@editround');
    route::post('/main/rounds/{round_id}/edit/process','RoundsController@editroundprocess');


    route::get('/main/mail/test','MainController@mailtest');

//rutas de user
    route::get('main/users','UserController@lists');
    route::get('main/users/add','UserController@add');
    route::post('main/users/add/process','UserController@create');
    route::get('main/users/getdata','UserController@getData');
    route::get('main/users/{user_id}','UserController@detail');
    route::post('main/users/{user_id}/roles/save','UserController@addroles');
    route::post('main/users/{user_id}/password/change','UserController@changepassword');
    route::post('main/users/{user_id}/edit/process','UserController@editprocess');


    route::get('main/companys','CompanysController@lists');
    route::get('main/companys/add','CompanysController@add');
    route::post('main/companys/add/process','CompanysController@addprocess');
    route::get('main/companys/getdata','CompanysController@getdata');
    route::get('main/companys/{company_id}','CompanysController@details');

    route::get('main/incidents','IncidentsWebController@lists');
    route::get('main/incidents/getdata','IncidentsWebController@getdata');
    route::get('main/incidents/{incident_id}','IncidentsWebController@details');
    route::get('main/incidents/{incident_id}/approve','IncidentsWebController@approved');
    route::get('main/incidents/{incident_id}/generatereport','IncidentsWebController@generatereport');


    route::get('main/truckexits','TruckexitsController@lists');
    route::get('main/truckexits/getdata','TruckexitsController@getdata');
    route::get('main/truckexits/{incident_id}','TruckexitsController@details');
    route::get('main/truckexits/{incident_id}/approve','TruckexitsController@approved');
    route::get('main/truckexits/{incident_id}/generatereport','TruckexitsController@generatereport');
    route::get('main/truckexits/{incident_id}/edit','TruckexitsController@edit');
    route::post('main/truckexits/{incident_id}/edit/process','TruckexitsController@editprocess');



});

route::get('main/documents/truckexits/{incident_id}/{key}','TruckexitsController@publicdocument');
route::get('main/documents/incidents/{incident_id}/{key}','IncidentsWebController@publicdocument');
route::get('main/checkdocuments/','IncidentsWebController@checkdocuments');


