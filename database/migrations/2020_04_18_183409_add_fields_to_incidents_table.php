<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToIncidentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incidents', function (Blueprint $table) {
            $table->time('inside_hour')->nullable();
            $table->time('exit_hour')->nullable();
            $table->string('desinfection_document')->nullable();
            $table->string('driver')->nullable();
            $table->string('guide_number')->nullable();
            $table->string('bins')->nullable();
            $table->string('kilograms')->nullable();
            $table->string('origin')->nullable();
            $table->string('destination')->nullable();
            $table->string('sails')->nullable();
            $table->string('phone')->nullable();
            $table->string('comentary')->nullable();
            $table->string('key')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incidents', function (Blueprint $table) {
            $table->dropColumn(['inside_hour','exit_hour','desinfection_document','driver','guide_number','bins','kilograms','origin','destination','sails','phone','comentary','key']);
        });
    }
}
