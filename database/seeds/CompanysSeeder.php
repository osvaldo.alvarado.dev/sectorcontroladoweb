<?php

use Illuminate\Database\Seeder;

use App\Companys;

class CompanysSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $element = new Companys(); $element->name='Compania de Testing 1'; $element->save();
        $element = new Companys(); $element->name='Empresa de Ruben SA'; $element->save();
    }
}
