<?php

namespace App\Http\Controllers;

use App\Marks;
use App\Roundcards;
use Illuminate\Http\Request;
use App\Cards;
use App\Rounds;
use App\Rounddays;
use DB;
use App\Instances;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class MarksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $marks = Marks::all();


        return $marks;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        //Instanciamos la clase Pokemons
        $mark = new Marks;
        //Declaramos el nombre con el nombre enviado en el request

        //obtenemos la tarjeta donde sea igual al nombre enviado
        $card = Cards::where('name','=',$request->card_id)->first();


        //busca si la tarjeta existe
        if($card){


            //instancia la tarjeta por que existe
            $mark->card_id = $card->id;

            //busca en todas las rondas donde se encuentre esta tarjeta
            $roundscards = Roundcards::where('card_id','=',$card->id)->pluck('round_id')->toArray();


            $minutes_to_delay = 1;

            if(isset($roundscards) && count($roundscards) >= 1){
                //concatena un string todas las rondas donde aparece esta tarjeta
                $roundscards = join(",",$roundscards);


                //busca si existe alguna ronda entre la hora establecida de la ronda establecida
                $round = Db::select('select *,
                            TIME(NOW()) - INTERVAL '.$minutes_to_delay.' minute as desde,
                            TIME(NOW()) + INTERVAL '.$minutes_to_delay.' minute as hasta
                            from rounds
                            where 
                            rounds.enabled = 1             
                            and TIME(NOW()) >= rounds.hour_to_round and TIME(NOW()) <= rounds.hour_to_round_finish
                            and id in      
                        ('.$roundscards.')');


            }




            //si existe rondas para esta tarjeta, busca si aplica para el dia de hoy para marcarla
            if(isset($round[0])){
                //obtiene solo la primera ronda encontrada
                $roundday = Db::select('select * from rounddays

                where  rounddays.enabled = 1 and day_number = DAYOFWEEK(NOW()) and round_id =
                '.$round[0]->id);
            }


            //si encontro la ronda la asocia a la tarjeta la marca
            if(isset($roundday[0])){
                $mark->roundday_id = $roundday[0]->id;
            }

            if(isset($input['latitude']) and isset($input['longitude'])){
                $mark->latitude = $input['latitude'];
                $mark->longitude = $input['longitude'];
            }

            //Guardamos el cambio en nuestro modelo
            $mark->save();

            //nombre de la tarjeta controlada
            $mark['card_title'] = $card->title;

            //si la ronda del dia aplica le ingresa el nombre de la ronda en cuestion a la marka para devolverla
            if(isset($roundday[0])){
                $mark['round_name'] = $round[0]->name;
            }

            return $mark;
        }else{
            return response()->json([
                'error' => 'Esta credencial no existe en el sistema'], 400);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Marks  $marks
     * @return \Illuminate\Http\Response
     */
    public function show(Marks $marks)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Marks  $marks
     * @return \Illuminate\Http\Response
     */
    public function edit(Marks $marks)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Marks  $marks
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marks $marks)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Marks  $marks
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marks $marks)
    {
        //
    }


    public function items(Request $request){

        if(Auth::user()->hasRole(['superadministrador'])){
            $instances = Instances::all();
        }else{
            $instances = Instances::where('company_id','=',Auth::user()->company_id)->get();
        }

        $input = $request->all();


        if(isset($input['instance_id'])) {

            $from = Carbon::parse($request->input('from'))->startOfDay();
            $to = Carbon::parse($request->input('to'))->endOfDay();

            $marks = Db::select("
                        select marks.id as mark_id,
                        cards.title as card_title,
                        marks.created_at as created_at,
                        rounds.name as round_name,
                        rounds.hour_to_round as hour_to_round,
                        rounds.hour_to_round_finish as hour_to_round_finish
                        from marks 
                        left join cards on marks.card_id = cards.id
                        left join rounddays on marks.roundday_id = rounddays.id
                        left join rounds on rounddays.round_id = rounds.id
                        where marks.created_at
                       
                        >= '".$from."' and  marks.created_at <='".$to."'
                        and
                        cards.instance_id =".$input['instance_id']."
                        order by marks.id asc
                        ");



            $instance_selected_id = $input['instance_id'];
            $instance_selected = Instances::find($input['instance_id']);


            return view('marks.list',compact('marks','instances','instance_selected_id','from','to','instance_selected'));
        }else{
            $from = Carbon::now();
            $to = Carbon::now();

            return view('marks.list',compact('instances','from','to'));
        }



    }



    public function itemsByDay(Request $request){

        if(Auth::user()->hasRole(['superadministrador'])){
            $instances = Instances::all();
        }else{
            $instances = Instances::where('company_id','=',Auth::user()->company_id)->get();
        }

        $input = $request->all();


        if(isset($input['instance_id'])) {

            $from = Carbon::parse($request->input('from'))->startOfDay();

            /*
            $marks = Db::select("
                       select *,
                            (select count(distinct marks.card_id) as cantidad_rondas 
                            
                            from marks where marks.roundday_id = rounddays.id
                            and DAYOFWEEK(marks.created_at) =  DAYOFWEEK('".$from."')
                            
                            ) as sectores_controlados,
                            (select count(*) as cantidad_tarjetas from roundcards where roundcards.round_id = rounds.id) as sectores_a_controlar
                            from rounddays
                            left join rounds on rounddays.round_id = rounds.id
                        where 

                        day_number = DAYOFWEEK('".$from."') and
                        instance_id =".$input['instance_id']."
                         order by hour_to_round
                        ");

            */


            $day_of_week = ['','Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];

            $round_enabled = false;

            if(isset($input['round_enabled'])){
                $round_enabled = true;
                $marks = Db::select("
                    
                              select *,rounddays.id as roundday_id,
                                        (select count(distinct marks.card_id) as cantidad_rondas 
                                        from marks where marks.roundday_id = rounddays.id
                                        and WEEK(marks.created_at,1) =  WEEK('".$from."',1) 
                                        and DAYOFWEEK(marks.created_at) = rounddays.day_number
                                        and YEAR(marks.created_at) = YEAR('".$from."')
                                        ) as sectores_controlados,
                                        (select count(*) as cantidad_tarjetas from roundcards where roundcards.round_id = rounds.id) as sectores_a_controlar
                                        from rounddays
                                        left join rounds on rounddays.round_id = rounds.id
                                        where 
                                        /*rounddays.enabled = 1 and */
                                        instance_id =".$input['instance_id']."
                                        and rounddays.enabled = 1
                                        and rounds.enabled = 1 
                              order by hour_to_round
                        ");
            }else{
                $marks = Db::select("
                    
                              select *,rounddays.id as roundday_id,
                                        (select count(distinct marks.card_id) as cantidad_rondas 
                                        from marks where marks.roundday_id = rounddays.id
                                        and WEEK(marks.created_at,1) =  WEEK('".$from."',1) 
                                        and DAYOFWEEK(marks.created_at) = rounddays.day_number
                                        and YEAR(marks.created_at) = YEAR('".$from."')
                                        ) as sectores_controlados,
                                        (select count(*) as cantidad_tarjetas from roundcards where roundcards.round_id = rounds.id) as sectores_a_controlar
                                        from rounddays
                                        left join rounds on rounddays.round_id = rounds.id
                                        where 
                                        /*rounddays.enabled = 1 and */
                                        instance_id =".$input['instance_id']."
                                        and rounddays.enabled = 1
                              order by hour_to_round
                        ");
            }



            $instance_selected_id = $input['instance_id'];
            $instance_selected = Instances::find($input['instance_id']);


            return view('marks.listbyday',compact('marks','instances','instance_selected_id','from','to','instance_selected','day_of_week','round_enabled'));
        }else{
            $from = Carbon::now();

            $round_enabled = true;
            return view('marks.listbyday',compact('instances','from','to','round_enabled'));
        }



    }



    public function getByDateAndRound($date_string,$round_id,$day_number){

        $round = Rounds::find($round_id);






        $marks = Db::select("
         
                              select *,
        
         (select count(distinct marks.card_id) as cantidad_rondas 

                from marks
                where marks.roundday_id = rounddays.id
                and DAYOFWEEK(marks.created_at) = ".$day_number."
                and marks.card_id = cards.id
                and YEAR(marks.created_at) = YEAR('".$date_string."')
                  and WEEK(marks.created_at,1) = WEEK('".$date_string."',1)
                ) as sector_controlado,
                (select marks.created_at as cantidad_rondas 
                from marks
                where marks.roundday_id = rounddays.id
                and DAYOFWEEK(marks.created_at) = ".$day_number."
                and marks.card_id = cards.id
                and YEAR(marks.created_at) = YEAR('".$date_string."')
                and WEEK(marks.created_at,1) = WEEK('".$date_string."',1)
                limit 1
                ) as fecha_control
                
                from roundcards
                left join cards on roundcards.card_id = cards.id
                left join rounddays on rounddays.round_id = roundcards.round_id and rounddays.day_number = ".$day_number."
                where roundcards.round_id = ".$round_id."
        ");

       // return json_encode($marks);

        return view('marks.resumeperday',compact('marks','round'));
    }
}
