<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Validator;
use Auth;
use DB;
use Log;
use App\Instances;
use App\Marks;
use App\LoginLogs;
use App\Mail\SendMailable;
use Mail;



class MainController extends Controller
{
    function checkLogin(Request $request){

        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required|min:3'
        ]);

        $user_data = array (
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        );

        if(Auth::attempt(['email' => $user_data['email'], 'password' => $user_data['password'], 'status' => 1])){

            $login_log = new LoginLogs();
            $login_log->ip = $request->ip();
            $login_log->message= "Inicio Correcto de Sesion ".$request->get('email');
            $login_log->user_id = Auth::user()->id;
            $login_log->success =  true;
            $login_log->save();

            return redirect('main/home');
        }
        else{

            $login_log = new LoginLogs();
            $login_log->ip = $request->ip();
            $login_log->message= "Inicio Incorrecto de sesion mail:".$request->get('email')." clave: ".$request->get('password');
            $login_log->success =  false;
            $login_log->save();
            return back()->with('error','Error en las credenciales');
        }
    }


    function home(){

        //obtiene las intancias necesarias, solo los id para despues utilizarlo en la query que obtendra los markajes
        if(Auth::user()->hasRole(['superadministrador'])){
            $instances_array = Instances::all()->pluck('id')->toArray();;
        }else{
            $instances_array = Instances::where('company_id','=',Auth::user()->company_id)->get()->pluck('id')->toArray();;;
        }




        $instances_to_query = join(",",$instances_array);

        //verifica que el usuario tenga instancias asociadas
        if(count($instances_array) > 0){
            $marks = Db::select('select count(*) as marks_quantity,instances.name as instance_name from marks
                            left join cards on marks.card_id = cards.id
                            left join instances on cards.instance_id = instances.id
                            
                            where instances.enabled
                            and instances.id in 
                          
                          ('.$instances_to_query.')   
                            group by instances.name ');
        }else{
            $marks = [];
        }


        return view('pages.home',compact('marks'));
    }


    function logout()
    {
        Auth::logout();
        return redirect('/');
    }


    function mailtest()
    {
        ini_set('max_execution_time', 1500);



        $variable = "Etceeetera";

        $instances = Instances::all();


        $day_of_week = ['','Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];

        ini_set('max_execution_time', 1500);


        foreach($instances as $instance){
            $marks = Db::select("
                        
                                select *,
                                        (select count(distinct marks.card_id) as cantidad_rondas 
                                        
                                        from marks where marks.roundday_id = rounddays.id
                                        and DAYOFYEAR(marks.created_at) = DAYOFYEAR(CURDATE())
                                        and YEAR(marks.created_at) = YEAR(CURDATE())
                                        ) as sectores_controlados,
                                        (select count(*) as cantidad_tarjetas from roundcards where roundcards.round_id = rounds.id) as sectores_a_controlar
                                        from rounddays
                                        left join rounds on rounddays.round_id = rounds.id
                                        where 
                                        /*rounddays.enabled = 1 and */
                                    
                                        instance_id =".$instance->id."
                                        and rounddays.enabled = 1
                                        and rounddays.day_number = DAYOFWEEK(CURDATE()) 
                                        and rounds.hour_to_round_finish < CURTIME()
                                order by hour_to_round
                        ");


            $when = now()->addSeconds(2);

            $sendMailable = New SendMailable();

            $sendMailable::later(5,'osvaldo.alvarado.dev@gmail.com');
        }

        return "OKA";
    }


    public function hello(){
        //funcion para retornar un status code funcional
        return response()->json([
            'ok' => 'Todo funcionando correctamente'], 200);
    }
}
