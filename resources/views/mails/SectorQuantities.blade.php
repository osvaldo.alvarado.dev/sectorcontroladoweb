<div>
    Alerta enviada: {{date('d-m-Y H:i:s')}}<br/>
    Existen rondas incompletas en <b>{{$variable}}</b><br/>
    Ronda : {{$selected_rounds_to_alert[0]->name}}  <br/>
    Horario desde : <b>{{$selected_rounds_to_alert[0]->hour_to_round}}</b> hasta: <b>{{$selected_rounds_to_alert[0]->hour_to_round_finish}}</b>
    <br>


    Sectores Controlados : {{$selected_rounds_to_alert[0]->sectores_controlados}} / {{$selected_rounds_to_alert[0]->sectores_a_controlar}}<br/><br/>


    @foreach($controled_sectors as $card)
        Sector: <b>{{$card->title}}</b>
        @if($card->sector_controlado)
            <b style="color:limegreen">Controlado</b>
        @else
            <b style="color:red">No Controlado</b>
        @endif
        <br/>
    @endforeach
</div>