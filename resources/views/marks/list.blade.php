
@extends('containers.maincontainer')

@section('content')

    <style>


        @page {
            size: auto;  /* auto is the initial value */
            margin: 0mm; /* this affects the margin in the printer settings */
        }
        @media print {
            #printPageButton {
                display: none;
            }

            canvas.chart-canvas {
                min-height: 100%;
                max-width: 100%;
                max-height: 100%;
                height: auto!important;
                width: auto!important;
            }
        }
    </style>

    <div class="box">


        <div class="box-body">
            <h2 class="page-header">
                <i class="fa fa-edit"></i>

                @if(isset($marks))
                    @if(date('Y-m-d', strtotime($from)) == date('Y-m-d', strtotime($to)))
                        Sectores controlados en {{$instance_selected->name}} el dia : {{date('Y-m-d', strtotime($from))}}
                    @else
                        Sectores controlados {{$instance_selected->name}} desde: {{date('Y-m-d', strtotime($from))}} hasta: {{date('Y-m-d', strtotime($from))}}
                    @endif

                @else
                    Sectores controlados
                @endif
            </h2>

            <div id="printPageButton">
            <form method="get">
                {{csrf_field()}}

                <div class="col-12">
                    <label for="exampleInputEmail1">Seleccione una instancia mas abajo</label>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <label for="exampleInputEmail1">Desde:</label>
                        <input required type="date" class="form-control" name="from" value="{{isset($from) ? date('Y-m-d', strtotime($from)) : '' }}" placeholder="">
                    </div>
                    <div class="col-xs-12">
                        <label for="exampleInputEmail1">Hasta:</label>
                        <input required type="date" class="form-control" value="{{isset($to) ? date('Y-m-d', strtotime($to)) : '' }}" name="to" placeholder="">
                    </div>
                </div>

                <br/>



                <div class="col-12">
                    <label for="exampleInputEmail1">Seleccione instancia:</label>
                    <select required class="form-control" id="instance_id" name="instance_id">
                        <option value="">Seleccione una instancia</option>
                        @foreach($instances as $instance)

                            <option {{isset($instance_selected_id) ? $instance->id == $instance_selected_id ? 'selected' : '' : '' }} value="{{$instance->id}}"> {{$instance->name}} </option>

                        @endforeach
                    </select>
                </div>
                <br />
                <div class="col-12">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Filtrar</button>
                    <button class='btn btn-primary' id="printPageButton" onClick="window.print();"><i class="fa fa-print"></i> Imprimir</button>
                </div>
            </form>
            </div>
            <br/>

            @if(isset($marks))
                <div class="col-12">

                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                <th>ID</th>
                                <th>Fecha</th>
                                <th>Area</th>
                                <th>Ronda</th>
                            </tr>
                            @foreach($marks as $mark)
                                <tr>
                                    <td>
                                        @if(isset($mark->round_name))
                                            <span class="badge bg-green">{{$mark->mark_id}}</span>
                                        @else
                                            <span class="badge bg-yellow">{{$mark->mark_id}}</span>
                                        @endif

                                    </td>
                                    <td>{{$mark->created_at}}</td>
                                    <td>{{$mark->card_title}}</td>
                                    <td>
                                        @if(isset($mark->round_name))
                                            <span class="badge bg-green">   {{$mark->round_name}} <br/> Desde: {{$mark->hour_to_round}} -  Hasta: {{$mark->hour_to_round_finish}} </span>
                                        @else
                                            <span class="badge bg-yellow">Sin Ronda Especifica</span>
                                        @endif

                                    </td>

                                </tr>

                            @endforeach
                            </tbody></table>
                    </div>
                </div>

            @endif

    </div>
@stop