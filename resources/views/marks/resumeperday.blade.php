<body>

    <p>
        Ronda : <b>{{$round->name}}</b><br/>
        Desde: <b>{{$round->hour_to_round}}</b> Hasta : <b>{{$round->hour_to_round_finish}}</b>
    </p>

    @foreach($marks as $mark)
        <p>
        Sector: <b>{{$mark->title}}</b>
        @if($mark->sector_controlado)
            <b style="color:limegreen">Controlado </b> <br/><i class="fa fa-clock-o"></i> {{ date('d-m-Y H:i:s', strtotime($mark->fecha_control))}}
        @else
            <b style="color:red">No Controlado</b>
        @endif

        </p>
    @endforeach
</body>