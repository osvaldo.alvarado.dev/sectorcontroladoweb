<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instances extends Model
{
    protected $fillable = ['id','name','company_id','address','lat','long','key'];



    public function cards(){
        return $this->hasMany('App\Cards','instance_id','id');
    }


    public function rounds(){
        return $this->hasMany('App\Rounds','instance_id','id');
    }

    public function incidents(){
        return $this->hasMany('App\Incidents','instance_id','id');
    }


    public function hasCard($card){
        //verifica que sea un array, si es un array hace una busqueda del array
        if (is_array($card)) {
            return null !== $this->cards()->whereIn('id', $card)->first();
        }else{
            return null !== $this->cards()->where('id', $card)->first();
        }
    }


    public function company(){
        return $this->hasOne('App\Companys','id','company_id');
    }
}
