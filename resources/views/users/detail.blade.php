
@extends('containers.maincontainer')

<style>


    @page {
        size: auto;  /* auto is the initial value */
        margin: 0mm; /* this affects the margin in the printer settings */
    }
    @media print {
        #printPageButton {
            display: none;
        }
    }
</style>

@section('content')
    <div class="box">
        <div class="box-body">
            <h2 class="page-header">
                <i class="fa fa-list"></i> Detalles del Usuario: {{$user->name}}<b></b>
            </h2>
            <form method="post" action="{{url('main/users/'.$user->id.'/edit/process')}}">
                {{csrf_field()}}


                <div class="form-group">
                    <label for="exampleInputEmail1">Nombre </label>
                    <input required type="text" class="form-control" name="name" value="{{$user->name}}">
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Mail</label>
                    <input required type="email" class="form-control" name="email"  value="{{$user->email}}">
                </div>

                <div class="form-group">

                        <div class="checkbox">
                            <label>
                                @if($user->status)
                                    <input type="checkbox" name="status" checked> Activo
                                @else
                                    <input type="checkbox" name="status"> <b>Activo</b>
                                @endif
                            </label>
                        </div>

                </div>



                <div class="form-group">
                    <label for="exampleInputEmail1">Empresa :</label>
                    <select required class="form-control selectpicker" data-live-search="true" name="company_id" id="company_id">
                        <option value="0"> Sin Instancia </option>
                        @foreach($companys as $company)
                            @if (old('company_id') == $company->id)
                                <option selected value="{{$company->id}}"> {{$company->name}} </option>
                            @else
                                <option {{isset($user->company_id) ? $user->company_id == $company->id ? 'selected' : '' : '' }} value="{{$company->id}}"> {{$company->name}} </option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>Guardar</button>
            </form>

        </div>

    </div>


    <div class="box">
        <div class="box-body">
            <h2 class="page-header">
                <i class="fa fa-list"></i> Permisos del usuario: {{$user->name}}<b></b>
            </h2>
            <div class="col-sm-6 invoice-col">

                <form method="post" action="{{url('main/users/'.$user->id.'/roles/save')}}">
                    {{csrf_field()}}
                <div class="form-group">
                    <label for="exampleInputEmail1"></label>
                    @foreach($roles as $role)
                        <div class="form-check">
                            @if($user->hasRole($role->name))
                                <input class="form-check-input" checked="true" type="checkbox" value="{{$role->id}}" name="roles[]">
                            @else
                                <input class="form-check-input" type="checkbox" value="{{$role->id}}" name="roles[]">
                            @endif
                            <label class="form-check-label" for="defaultCheck1">
                                {{$role->name}}
                            </label>
                        </div>
                    @endforeach
                </div>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>Actualizar Permisos</button>
                </form>
            </div>

        </div>

    </div>


    <div class="box">
        <div class="box-body">
            <h2 class="page-header">
                <i class="fa fa-list"></i> Cambiar Clave del usuario: {{$user->name}}<b></b>
            </h2>
            <div class="col-sm-6 invoice-col">

                <form method="post" action="{{url('main/users/'.$user->id.'/password/change')}}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Contraseña</label>
                            <input required type="text" class="form-control" name="password" placeholder="">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>Cambiar Clave</button>
                </form>
            </div>

        </div>

    </div>





@stop