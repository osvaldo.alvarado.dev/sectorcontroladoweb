
@extends('containers.maincontainer')

@section('content')

    <div class="box">
        <div class="box-body">
            <h2 class="page-header">
                <i class="fa fa-id-card-o"></i> Listado Usuarios:
            </h2>
            <a  href="{{ url('/') }}/main/users/add" class="btn btn-success"><i class="fa fa-plus"></i>Añadir Nuevo Usuario</a>
            <br>      <br>
            <div class="table-responsive">
                <table id="desinfections_table" class="table table-bordered table-hover dataTable">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Estado</th>
                        <th>Detalles</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/af-2.3.0/b-1.5.2/b-colvis-1.5.2/b-flash-1.5.2/b-html5-1.5.2/b-print-1.5.2/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.4.0/r-2.2.2/rg-1.0.3/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>
    <script src="{{ url('/') }}/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#desinfections_table').DataTable({
                responsive: true,
                fixedHeader: {
                    headerOffset: 50
                },



                "processing" : true,
                "serverSide" : true,

                "ajax" : "{{ url('/') }}/main/users/getdata",
                "columns" : [
                    {"data" : "id"},
                    {"data" : "name"},
                    {
                        "render": function ( data, type, full, meta ) {

                            returnString = "";
                            if(full.status == 0){
                                returnString ="<span class=\"badge bg-red\"><i class='fa fa-close'></i> Inactivo</span>";
                            }else{
                                returnString ="<span class=\"badge bg-green\"><i class='fa fa-check'></i> Activo</span>";
                            }

                            return returnString;
                        }
                    },
                    {
                        "render": function ( data, type, full, meta ) {

                            var returnString = "<div class=\"btn-group\"> " +
                                " <button type=\"button\" class=\"btn btn-success dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">Opciones " +
                                "<span class=\"caret\"></span><span class=\"sr-only\">Toggle Dropdown</span></button>";

                            returnString = returnString + "  <ul class=\"dropdown-menu dropdown-menu-right primary\" role=\"menu\">";
                            returnString = returnString + "<li><a href='{{ url('/')}}/main/users/"+full.id+"'> <i class='fa fa-search'></i>Detalle</a></li>";

                            returnString = returnString + "</ul></div>";

                            returnString = "<a class='btn btn-success' href='{{ url('/')}}/main/users/"+full.id+"'> <i class='fa fa-search'></i>Detalle</a>";

                            return returnString;
                        }
                    },

                ],
                language: {
                    "lengthMenu": "Mostrar _MENU_ registros por pagina &nbsp;&nbsp;&nbsp;",
                    "zeroRecords": "No se encuentra ningun registro",
                    "info": "Pagina _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros",
                    "infoFiltered": "(buscando entre _MAX_ registros)",
                    "search":         "Filtrar Registros : &nbsp",
                    "processing" : "Cargando...",
                    paginate: {
                        first:      "Primera Pagina",
                        previous:   "Anterior",
                        next:       "Siguiente",
                        last:       "Ultima"
                    },

                },
                "order": [[ 0, "desc" ]]
            })
        });

    </script>

@stop