<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cards extends Model
{
    protected $fillable = ['id','name','title','instance_id'];


    public function marks(){
        return $this->hasMany('App\Marks','card_id','id');
    }
}
