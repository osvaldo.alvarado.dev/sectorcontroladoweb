
@extends('containers.maincontainer')

@section('content')
    <div class="box">


        <div class="box-body">
            <h2 class="page-header">
                <i class="fa fa-pencil"></i> Crear Nueva Empresa:
            </h2>

            <form method="post" action="{{url('main/companys/add/process')}}">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="exampleInputEmail1">Nombre de la Empresa: </label>
                    <input required type="text" class="form-control" name="name" value="{{isset($company->name) ? $company->name : '' }}" required>
                </div>

                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>Guardar</button>
            </form>
        </div>


    </div>
@stop