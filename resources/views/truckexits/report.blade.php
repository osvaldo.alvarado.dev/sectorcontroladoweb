<style type="text/css">
    .main{
        font-family: Verdana, Arial, sans-serif;

    }
    #content{
        position:absolute;
        z-index:1;
    }

    .title{
        text-align:center;
        font-weight: bold;
        font-size:20px;
    }

    #bg-text
    {
        color:lightgrey;
        font-size:120px;
        transform:rotate(300deg);
        -webkit-transform:rotate(300deg);
    }

    .cajon_circular{
        border-radius: 0px;
        border-style:solid;
        border-width: thin;
        padding:5px;
    }


    #background{
        position:absolute;
        z-index:-10;
        background:white;
        display:block;
        min-height:50%;
        min-width:50%;
        color:yellow;
    }

    .cajon_fotografias{

    }

    .cajon_fotografias img {

    }

    .img_box {
        width: 750px;
        height: 400px;
        text-align: center;
        margin-top:10px;
        margin-bottom: 25px;
    }

    .img_box img {

        max-height: 400px;

    }

    footer {
        position: fixed;
        bottom: -50px;
        left: 0px;
        right: 0px;
        height: 50px;
        font-family: Verdana, Arial, sans-serif;
        font-size:12px;
        /** Extra personal styles **/
        background-color: rgb(255, 102, 95);
        color: white;
        text-align: center;
        line-height: 35px;
    }

    .cajon_fotografias footer {
        position: fixed;
        bottom: -50px;
        left: 0px;
        right: 0px;
        height: 50px;
        font-family: Verdana, Arial, sans-serif;
        font-size:12px;
        /** Extra personal styles **/
        background-color: rgb(255, 102, 95);
        color: white;
        text-align: center;
        line-height: 35px;
    }
    header {
        position: fixed;
        top: -60px;
        left: 0px;
        right: 0px;
        height: 50px;

        /** Extra personal styles **/
        background-color: rgba(255, 8, 0, 0.7);
        color: white;
        text-align: center;
        line-height: 35px;
    }


    #watermark
    {
        position:fixed;
        bottom:1000px;
        left: 580px;
        opacity:1;
        z-index:99;
        color:black;
    }
</style>



<div class="main" id="content">
    <div id="watermark">
        {{--md5(microtime())--}}
        <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')
                        ->size(120)->errorCorrection('H')
                        ->generate('http://app.sectorcontrolado.cl/main/documents/truckexits/'.$incident->id.'/'.$incident->key)) !!} ">
    </div>
    @if(!$incident->approved)
        <div id="background">
            <p id="bg-text">INFORME NO APROBADO</p>
        </div>
    @endif

    <div class="title" >
        Despacho de Camión {{date('d/m/Y', strtotime($incident->created_at))}}
    </div>
    <div class="cajon_circular">
        <strong>Folio: </strong> {{$incident->id}}<br>
        <strong>Fecha: </strong> {{date('d/m/Y', strtotime($incident->created_at))}}<br>
        <strong>Rut Chover: </strong> {{$incident->guard}}<br>
        <strong>Patente Rampla: </strong> {{$incident->title}}<br>
        <strong>Patente Camion: </strong> {{$incident->description}}<br>
        <strong>Hora de Entrada: </strong> {{$incident->inside_hour}}<br>
        <strong>Hora de Salida: </strong> {{$incident->exit_hour}}<br>
        <strong>Certificado Desinfección: </strong> {{$incident->desinfection_document}}<br>
        <strong>Nombre Conductor: </strong> {{$incident->driver}}<br>
        <strong>Numero de guia: </strong> {{$incident->guide_number}}<br>
        <strong>Bins: </strong> {{$incident->bins}}<br>
        <strong>Kilogramos: </strong> {{$incident->kilograms}}<br>
        <strong>Origen: </strong> {{$incident->origin}}<br>
        <strong>Destino: </strong> {{$incident->destination}}<br>
        <strong>Sellos: </strong> {{$incident->sails}}<br>
        <strong>Telefono: </strong> {{$incident->phone}}<br>
        <strong>Comentario: </strong> {{$incident->comentary}}<br>
    </div>


<footer>{{isset($incident->instance->company->url) ? $incident->instance->company->url : $incident->instance->company->name}} -  www.sectorcontrolado.cl</footer>


        <div class="cajon_fotografias">
            <br/>
            <b>Fotografias:</b><br/>
            <br/>
            <?php $ct = 0?>
            @foreach($incident->pictures as $picture)
                <?php $ct = $ct + 1;?>
                {{--Verifica que sea multiplo de dos para aplicar salto de pagina--}}
                <!--<div style="border-style: solid;  border-width: thin; margin-bottom: 5px">-->
                <div>
                    <div>
                    <span>
                        Id Fotografia: <b>{{$picture->id}}</b><br/>
                    </span>
                    </div>
                    <div class="img_box">
                        <img src="{{$picture->uri}}">
                    </div>
                </div>
                {{--Solo si las fotografias son mayores a 1 aplica el salto de linea--}}
                @if(count($incident->pictures) > 1)
                        @if($ct == 1)
                            <div style='page-break-before:always'></div>
                        @endif
                @endif

                {{--Si es el ultimo elemento no añade nada--}}
                @if($loop->last)

                @else
                        @if($ct > 2)
                            @if ($ct % 2 != 0)
                                <div style='page-break-before:always'></div>
                            @endif
                        @endif
                @endif

                <footer>{{isset($incident->instance->company->url) ? $incident->instance->company->url : $incident->instance->company->name}} -  www.sectorcontrolado.cl</footer>
            @endforeach
        </div>

</div>





