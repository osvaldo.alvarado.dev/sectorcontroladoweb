<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incidents extends Model
{

    protected $fillable = ['id','title','description','instance_id','incidenttype_id','approved','guard','inside_hour','exit_hour','desinfection_document','driver','guide_number','bins','kilograms','origin','destination','sails','phone','comentary','key'];

    public function incidenttype(){
        return $this->hasOne('App\Incidenttypes','id','incidenttype_id');
    }


    public function pictures(){
        return $this->hasMany('App\Pictures','incident_id','id');
    }

    public function instance(){
        return $this->hasOne('App\Instances','id','instance_id');
    }
}
