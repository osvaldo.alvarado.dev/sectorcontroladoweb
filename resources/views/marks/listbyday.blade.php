
@extends('containers.maincontainer')

@section('content')

    <style>


        @page {
            size: auto;  /* auto is the initial value */
            margin: 0mm; /* this affects the margin in the printer settings */
        }
        @media print {
            #printPageButton {
                display: none;
            }



            table {

                font-size: 8px;
                max-width: 2480px;
                width:100%;
            }

            table tbody td{
                width: auto;
                overflow: hidden;
                word-wrap: break-word;
            }

            .page-header{
                font-size:10px;
                text-align: center;
            }

            table tbody tr th {
                font-size:15px;
                text-align: center;
            }

            table tbody tr span:before {

              font-color :black;

            }

            .btn-success {
                font-size:11px;

            }

            .btn-danger {
                font-size:11px;

            }

            .btn-warning {
                font-size:11px;

            }
        }
    </style>

    <div class="box">


        <div class="box-body">
            <h2 class="page-header">
                <i class="fa fa-edit"></i>

                @if(isset($marks))
                    Sectores controlados semanalmente en:  <b>{{$instance_selected->name}}</b> el dia : {{date('Y-m-d', strtotime($from))}}   <small>www.sectorcontrolado.cl</small>
                @else
                    Sectores controlados semanalmente
                @endif
            </h2>

            <div id="printPageButton">
            <form method="get">
                {{csrf_field()}}

                <div class="col-12">
                    <label for="exampleInputEmail1">Seleccione una instancia mas abajo</label>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <label for="exampleInputEmail1">Desde:</label>
                        <input required type="date" class="form-control" name="from" value="{{isset($from) ? date('Y-m-d', strtotime($from)) : '' }}" placeholder="">
                    </div>

                </div>

                <br/>



                <div class="col-12">
                    <label for="exampleInputEmail1">Seleccione instancia:</label>
                    <select required class="form-control" id="instance_id" name="instance_id">
                        <option value="">Seleccione una instancia</option>
                        @foreach($instances as $instance)

                            <option {{isset($instance_selected_id) ? $instance->id == $instance_selected_id ? 'selected' : '' : '' }} value="{{$instance->id}}"> {{$instance->name}} </option>

                        @endforeach
                    </select>
                </div>

                <div class="col-12">
                    <label for="exampleInputEmail1">Solo rondas activas</label>

                    @if(isset($round_enabled))
                        @if($round_enabled)
                            <input type="checkbox" name="round_enabled" checked>
                        @else
                            <input type="checkbox" name="round_enabled">
                        @endif

                    @else
                        <input type="checkbox" name="round_enabled" checked>
                    @endif

                </div>



                <br />
                <div class="col-12">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Filtrar</button>
                    <button class='btn btn-primary' id="printPageButton" onClick="window.print();"><i class="fa fa-print"></i> Imprimir</button>
                </div>
            </form>
            </div>
            <br/>

            @if(isset($marks))
                <div class="col-12">

                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tbody><tr>
                                <th>Lunes</th>
                                <th>Martes</th>
                                <th>Miercoles</th>
                                <th>Jueves</th>
                                <th>Viernes</th>
                                <th>Sabado</th>
                                <th>Domingo</th>


                            </tr>

                        <tr>
                            <td>
                                @foreach($marks as $mark)

                                    @if($mark->day_number == 2)

                                        @if($mark->sectores_controlados == $mark->sectores_a_controlar)
                                            <button type="button" class="btn btn-success" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-check"></span>
                                                @elseif($mark->sectores_controlados == 0)
                                                    <button type="button" class="btn btn-danger" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-close"></span>
                                                        @else
                                                            <button type="button" class="btn btn-warning" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-warning" ></span>
                                                                @endif
                                                  {{$mark->name}} <br/>
                                                {{$mark->sectores_controlados}}  /     {{$mark->sectores_a_controlar}}<br/>
                                                <span class="fa fa-clock-o"></span> {{$mark->hour_to_round}}  -    {{$mark->hour_to_round_finish}}
                                              </button>
                                        <br/>
                                        <br/>

                                    @endif
                                @endforeach
                            </td>




                            <td>
                                @foreach($marks as $mark)

                                    @if($mark->day_number == 3)

                                        @if($mark->sectores_controlados == $mark->sectores_a_controlar)
                                            <button type="button" class="btn btn-success" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-check"></span>
                                                @elseif($mark->sectores_controlados == 0)
                                                    <button type="button" class="btn btn-danger" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-close"></span>
                                                        @else
                                                            <button type="button" class="btn btn-warning" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-warning" ></span>
                                                                @endif
                                                                {{$mark->name}} <br/>
                                                                {{$mark->sectores_controlados}}  /     {{$mark->sectores_a_controlar}}<br/>
                                                                <span class="fa fa-clock-o"></span> {{$mark->hour_to_round}}  -    {{$mark->hour_to_round_finish}}
                                                            </button>
                                                            <br/>
                                                            <br/>

                                        @endif
                                        @endforeach
                            </td>

                            <td>
                                @foreach($marks as $mark)

                                    @if($mark->day_number == 4)

                                        @if($mark->sectores_controlados == $mark->sectores_a_controlar)
                                            <button type="button" class="btn btn-success" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-check"></span>
                                                @elseif($mark->sectores_controlados == 0)
                                                    <button type="button" class="btn btn-danger" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-close"></span>
                                                        @else
                                                            <button type="button" class="btn btn-warning" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-warning" ></span>
                                                                @endif
                                                                {{$mark->name}} <br/>
                                                                {{$mark->sectores_controlados}}  /     {{$mark->sectores_a_controlar}}<br/>
                                                                <span class="fa fa-clock-o"></span> {{$mark->hour_to_round}}  -    {{$mark->hour_to_round_finish}}
                                                            </button>
                                                            <br/>
                                                            <br/>

                                        @endif
                                        @endforeach
                            </td>


                            <td>
                                @foreach($marks as $mark)

                                    @if($mark->day_number == 5)

                                        @if($mark->sectores_controlados == $mark->sectores_a_controlar)
                                            <button type="button" class="btn btn-success" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-check"></span>
                                                @elseif($mark->sectores_controlados == 0)
                                                    <button type="button" class="btn btn-danger" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-close"></span>
                                                        @else
                                                            <button type="button" class="btn btn-warning" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-warning" ></span>
                                                                @endif
                                                                {{$mark->name}} <br/>
                                                                {{$mark->sectores_controlados}}  /     {{$mark->sectores_a_controlar}}<br/>
                                                                <span class="fa fa-clock-o"></span> {{$mark->hour_to_round}}  -    {{$mark->hour_to_round_finish}}
                                                            </button>
                                                            <br/>
                                                            <br/>

                                        @endif
                                        @endforeach
                            </td>


                            <td>
                                @foreach($marks as $mark)

                                    @if($mark->day_number == 6)

                                        @if($mark->sectores_controlados == $mark->sectores_a_controlar)
                                            <button type="button" class="btn btn-success" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-check"></span>
                                                @elseif($mark->sectores_controlados == 0)
                                                    <button type="button" class="btn btn-danger" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-close"></span>
                                                        @else
                                                            <button type="button" class="btn btn-warning" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-warning" ></span>
                                                                @endif
                                                                {{$mark->name}} <br/>
                                                                {{$mark->sectores_controlados}}  /     {{$mark->sectores_a_controlar}}<br/>
                                                                <span class="fa fa-clock-o"></span> {{$mark->hour_to_round}}  -    {{$mark->hour_to_round_finish}}
                                                            </button>
                                                            <br/>
                                                            <br/>

                                        @endif
                                        @endforeach
                            </td>

                            <td>
                                @foreach($marks as $mark)

                                    @if($mark->day_number == 7)

                                        @if($mark->sectores_controlados == $mark->sectores_a_controlar)
                                            <button type="button" class="btn btn-success" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-check"></span>
                                                @elseif($mark->sectores_controlados == 0)
                                                    <button type="button" class="btn btn-danger" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-close"></span>
                                                        @else
                                                            <button type="button" class="btn btn-warning" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-warning" ></span>
                                                                @endif
                                                                {{$mark->name}} <br/>
                                                                {{$mark->sectores_controlados}}  /     {{$mark->sectores_a_controlar}}<br/>
                                                                <span class="fa fa-clock-o"></span> {{$mark->hour_to_round}}  -    {{$mark->hour_to_round_finish}}
                                                            </button>
                                                            <br/>
                                                            <br/>

                                        @endif
                                        @endforeach
                            </td>


                            <td>
                                @foreach($marks as $mark)

                                    @if($mark->day_number == 1)

                                            @if($mark->sectores_controlados == $mark->sectores_a_controlar)
                                                <button type="button" class="btn btn-success" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-check"></span>
                                            @elseif($mark->sectores_controlados == 0)
                                                <button type="button" class="btn btn-danger" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-close"></span>
                                            @else
                                                <button type="button" class="btn btn-warning" onclick="loadModal({{$mark->round_id}},{{$mark->day_number}})"> <span class="fa fa-warning" ></span>
                                            @endif

                                                    {{$mark->name}} <br/>
                                                    {{$mark->sectores_controlados}}  /     {{$mark->sectores_a_controlar}}<br/>
                                                    <span class="fa fa-clock-o"></span> {{$mark->hour_to_round}}  -    {{$mark->hour_to_round_finish}}
                                                </button>
                                                <br/>
                                                <br/>

                                        @endif
                                        @endforeach
                            </td>
                            </tr>

                            </tbody></table>
                    </div>
                </div>

            @endif
            <div class="modal fade" id="modal-default" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Sectores Controlados:</h4>
                        </div>
                        <div class="modal-body" id="modalbody">
                            <p>One fine body…</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>

                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
    </div>

<script>
    function loadModal(roundday_id,day_number){
        //muestra el modal

        @if(isset($from))
        var roundDate = "{{ date('Y-m-d', strtotime($from))}}";
        //var htmlString = "la cadena htmlllsss id: " + roundday_id + " dia:" + day_number + "Fecha" + roundDate;

        console.log("la cadena htmlllsss id: " + roundday_id + " dia:" + day_number + "Fecha" + roundDate);
        var htmlString = "Cargando...";
        //actualiza el contenido con el html obtenido
        $( '#modalbody' ).html( htmlString );

        //obtiene los parametros
        $.get(  "{{ url('/') }}/main/marks/bydateandround/" +roundDate + "/" + roundday_id + "/" + day_number, function( data ) {
            $( '#modalbody' ).html( data );
        });

        $('#modal-default').modal('show');


        @else

        @endif


    }


</script>

@stop