
@extends('containers.maincontainer')

@section('content')
    <div class="box">


        <div class="box-body">
            <h2 class="page-header">
                <i class="fa fa-pencil"></i> Detalles de la Instancia <b>{{$instance->name}}</b>
            </h2>

            <address>
                <b>Nombre: </b>{{$instance->name}}<br>
                <b>Direccion: </b>{{$instance->address}}<br>
                <b>Llave: </b>{{$instance->key}}<br>
                <b>Latitud: </b>{{$instance->lat}}<br>
                <b>Longitud: </b>{{$instance->long}}<br>
            </address>
            @if(Auth::user()->hasRole(['superadministrador']))
            <div class="row">
                <div class="col-xs-12"><a  href="{{ url('/') }}/main/instances/{{$instance->id}}/edit" class="btn btn-success"><i class="fa fa-edit"></i>Editar Instancia</a></div>
            </div>
            @endif
        </div>
    </div>


    <div class="box">
        <div class="box-body">
            <h2 class="page-header">
                <i class="fa fa-id-card"></i> Tarjetas Asociadas:</b>
            </h2>

            @if(Auth::user()->hasRole(['superadministrador']))
                <div>
                    <a  href="{{ url('/') }}/main/instances/{{$instance->id}}/addcard" class="btn btn-success"><i class="fa fa-plus"></i>Anadir Nueva Tarjeta</a>
                </div>
                <br />
            @endif





            <table class="table table-bordered">
                <tbody><tr>
                    <th style="width: 10px">Id</th>
                    <th>Numero de Serie</th>
                    <th>Titulo</th>

                </tr>

                @foreach($instance->cards as $card)
                    <tr>
                        <td>{{$card->id}}</td>
                        <td>{{$card->name}}</td>
                        <td>{{$card->title}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>





        </div>
    </div>


    <div class="box">
        <div class="box-body">
            <h2 class="page-header">
                <i class="fa fa-id-card"></i> Rondas Existentes:</b>
            </h2>

            @if(Auth::user()->hasRole(['superadministrador','administradorinstancia']))
                <div>
                    <a  href="{{ url('/') }}/main/instances/{{$instance->id}}/addround" class="btn btn-success"><i class="fa fa-plus"></i>Anadir Nueva Ronda</a>
                </div>
                <br />
            @endif
            <div class="col-xs-12 table-responsive">
            <table class="table table-bordered">
                <tbody><tr>
                    <th style="width: 10px">Id</th>
                    <th>Nombre</th>
                    <th>Estado</th>
                    <th>Hora Desde</th>
                    <th>Hora Hasta</th>
                    <th>Sectores</th>
                    <th>Dias</th>
                    <th>Mail</th>
                    <th>Opciones</th>
                </tr>
                @foreach($instance->rounds as $round)
                    <tr>
                        <td>{{$round->id}}</td>

                        <td>{{$round->name}}  </td>
                        <td>
                            @if($round->enabled)
                                <span class="badge bg-green"><i class="fa fa-check"></i> Activada </span>
                            @else
                                <span class="badge bg-red"><i class="fa fa-check"></i> Desactivada </span>
                            @endif
                        </td>
                        <td>{{$round->hour_to_round}}</td>
                        <td>{{$round->hour_to_round_finish}}</td>
                        <td>

                           @foreach($round->roundcards as $roundcard)

                                <span class="badge bg-yellow"><i class="fa fa-id-card"></i> {{$roundcard->card->title}} </span>
                           @endforeach
                        </td>

                        <?php $ct = 0;?>
                        <td>
                            @foreach($round->rounddays as $day)


                                    @if($day->enabled)
                                        <span class="badge bg-green"><i class="fa fa-check"></i> {{$days_of_week[$day->day_number]}} </span>
                                    @else
                                        <span class="badge bg-red"><i class="fa fa-close"></i> {{$days_of_week[$day->day_number]}} </span>

                                    @endif
                            @endforeach
                        </td>
                        <td>
                           @if($round->enable_alert)
                                <i class="fa fa-check"></i>
                           @else
                                <i class="fa fa-close"></i>
                            @endif
                        </td>

                        <td>
                            @if(Auth::user()->hasRole(['superadministrador','administradorinstancia']))
                                <a  href="{{ url('/') }}/main/rounds/{{$round->id}}/edit" class="btn btn-primary"><i class="fa fa-edit"></i>Editar Ronda</a>
                            @endif

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
@stop