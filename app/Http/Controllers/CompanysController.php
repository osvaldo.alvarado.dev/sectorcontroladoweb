<?php

namespace App\Http\Controllers;

use App\Companys;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;



class CompanysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Companys  $companys
     * @return \Illuminate\Http\Response
     */
    public function show(Companys $companys)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Companys  $companys
     * @return \Illuminate\Http\Response
     */
    public function edit(Companys $companys)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Companys  $companys
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Companys $companys)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Companys  $companys
     * @return \Illuminate\Http\Response
     */
    public function destroy(Companys $companys)
    {
        //
    }

    public function lists(){

        return view('companys.lists');
    }


    public function getdata(){
        Auth::user()->authorizeRoles(['superadministrador']);
        if( isset(Auth::user()->email)){
            $companys = Companys::all();

            return DataTables::of($companys)->make(true);
        }

    }

    public function details($company_id){

        $company = Companys::findOrFail($company_id);

        return view('companys.details',compact('company'));

    }


    public function add(){


        return view('companys.add');
    }


    public function addprocess(Request $request){
        $input = $request->all();


        $company = Companys::create($input);

        $returnUrl = url('/')."/main/companys";
        $sucess = true;
        return view('pages.genericprocess',compact('returnUrl','sucess'));

    }
}
