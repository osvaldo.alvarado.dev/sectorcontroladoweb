<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $dateFormat = 'Y-m-d H:i:s.v';

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
