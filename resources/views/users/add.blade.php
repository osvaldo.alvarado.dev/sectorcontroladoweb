
@extends('containers.maincontainer')

@section('content')
    <div class="box">


        <div class="box-body">
            <h2 class="page-header">
                <i class="fa fa-pencil"></i> Crear nuevo usuario
            </h2>
            <form method="post" action="{{url('main/users/add/process')}}">
                {{csrf_field()}}

                <input type="hidden" required type="text" class="form-control" value="1" name="status" placeholder="">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nombre Completo</label>
                    <input required type="text" class="form-control" name="name" placeholder="">
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Mail</label>
                    <input required type="email" class="form-control" name="email" placeholder="">
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Contraseña</label>
                    <input required type="text" class="form-control" name="password" placeholder="">
                </div>


                <div class="form-group">
                    <label for="exampleInputEmail1">Empresa :</label>
                    <select required class="form-control selectpicker" data-live-search="true" name="company_id" id="company_id">

                        @if(Auth::user()->hasRole(['superadministrador']))
                            <option value="0"> Sin Instancia </option>
                        @endif
                        @foreach($companys as $company)

                                <option {{isset($user->company_id) ? $user->company_id == $company->id ? 'selected' : '' : '' }} value="{{$company->id}}"> {{$company->name}} </option>

                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1"></label>
                    @foreach($roles as $role)
                        <div class="form-check">

                            <input class="form-check-input" type="checkbox" value="{{$role->id}}" name="roles[]">
                            <label class="form-check-label" for="defaultCheck1">
                                {{$role->name}}
                            </label>
                        </div>
                    @endforeach
                </div>

                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>Guardar</button>
            </form>
        </div>


    </div>
@stop