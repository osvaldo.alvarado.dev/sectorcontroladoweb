<?php

namespace App\Console\Commands;

use App\Rounddays;
use Illuminate\Console\Command;
use App\Mail\SendMailable;
use App\Instances;
use DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Mailgun\Mailgun;
use App\Roundalerts;
use Mockery\Exception;

class AlertSectors extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alert:sectors';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Alerta de los sectores en los cuales no se completo la ronda de las instancias que son notificables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        try {

        $variable = "Etceeetera";

        $instances = Instances::all();


        $day_of_week = ['','Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];

        ini_set('max_execution_time', 1500);


        foreach($instances as $instance) {

            try{
                $marks = Db::select(" select *,
                                (select count(distinct marks.card_id) as cantidad_rondas 
                                
                                from marks where marks.roundday_id = rounddays.id
                                and DAYOFYEAR(marks.created_at) = DAYOFYEAR(CURDATE())
                                and YEAR(marks.created_at) = YEAR(CURDATE())
                                ) as sectores_controlados,
                                (select count(*) as cantidad_tarjetas from roundcards where roundcards.round_id = rounds.id) as sectores_a_controlar
                                ,(select count(*) as cantidad_notificada_hoy from roundalerts where roundalerts.round_id = rounds.id 
                                and DATE(roundalerts.created_at) = CURDATE()
                                ) as ronda_notificada_hoy
                                from rounddays
                                left join rounds on rounddays.round_id = rounds.id
                                
                                where 
                                /*rounddays.enabled = 1 and */
                                    
                                        instance_id =" . $instance->id . "
                                        and rounddays.enabled = 1
                                        and rounddays.day_number = DAYOFWEEK(CURDATE()) 
                                        and rounds.hour_to_round_finish < CURTIME()
                                        and rounds.enable_alert = 1
                                        and rounds.enabled = 1
                                order by hour_to_round
                            ");

                //itera las marcas


                if (!empty($marks)) {


                    foreach ($marks as $mark) {
                        $selected_rounds_to_alert = New Collection();
                        $controled_sectors_collection = New Collection();

                        //pasa solo las rondas que tienen sectores pendientes a controlar y ademas no fue notificada
                        if ($mark->sectores_controlados < $mark->sectores_a_controlar && $mark->ronda_notificada_hoy == 0) {
                            //añade la ronda seleccionada a la coleccion ya que cumple con los criterios de busqueda
                            $selected_rounds_to_alert->add($mark);
                            //selecciona los sectores controlados de esta ronda
                            $controled_sectors = Db::select("
                                                        select *,
                                                        
                                                        (select count(distinct marks.card_id) as cantidad_rondas 
                                                        
                                                        from marks
                                                        where marks.roundday_id = rounddays.id
                                                        and DAYOFYEAR(marks.created_at) = DAYOFYEAR(CURDATE())
                                                        and marks.card_id = cards.id
                                                        and YEAR(marks.created_at) = YEAR(CURDATE())
                                                        ) as sector_controlado
                                                        from roundcards
                                                        left join cards on roundcards.card_id = cards.id
                                                        left join rounddays on rounddays.round_id = roundcards.round_id and rounddays.day_number = DAYOFWEEK(CURDATE())
                                                        where roundcards.round_id = " . $mark->round_id . "
                                                      ");

                            $controled_sectors_collection = collect($controled_sectors);
                            //instancia la alerta de la ronda para marcarla como notificada
                            $roundalert = New Roundalerts();
                            $roundalert->round_id = $mark->round_id;
                            $roundalert->enabled = 1;


                            //arma un array con los correos a los que hay que notificar
                            $mail_list = explode(";", $mark->mails_to_alert);

                            //valida que la lista de correos sea valida
                            $filterd_emails = array();
                            foreach($mail_list as $email) {
                                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {

                                } else {
                                    array_push($filterd_emails,$email);
                                }
                            }


                            //envia correo
                            Mail::to($filterd_emails)->send(new SendMailable($instance->name, $selected_rounds_to_alert, $controled_sectors_collection));
                            if(count(Mail::failures()) > 0){
                                log::error('No se pudo enviar correctamente el correo en '.$selected_rounds_to_alert[0]->name);
                                log::error($filterd_emails);
                            }else{
                                //log::info('MAILSEND: Se envio correctamente esta alerta'.$selected_rounds_to_alert[0]->name);
                                $roundalert->save();
                            }


                        } else {

                        }
                    }


                }


                }catch (\Exception $e) {
                    //report($e);
                    log::error('Error al tratar de notificar instancia: '.$instance->name);
                    log::error('Se registro un error en el ciclo de envio de datos : '.$e);
                }
            } //aqui cerrar

        } catch (\Exception $e) {
            //report($e);
            log::error('Se registro un error en el sistema : '.$e);
        }
    }

}
