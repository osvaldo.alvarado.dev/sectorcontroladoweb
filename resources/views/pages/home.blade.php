
@extends('containers.maincontainer')

@section('content')

    <section class="content-header">
        <h1>
            Bienvenido a SectorControlado
            <small>Tablero de Control</small>
        </h1>
    </section>


    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>150</h3>

                    <p>Puntos Controlados</p>
                </div>
                <div class="icon">
                    <i class="ion ion-checkmark"></i>
                </div>
                <a href="#" class="small-box-footer">Mas info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>12<sup style="font-size: 20px"></sup></h3>

                    <p>Incidencias Registradas</p>
                </div>
                <div class="icon">
                    <i class="fa fa-edit"></i>
                </div>
                <a href="#" class="small-box-footer">Mas info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>10</h3>

                    <p>Rondas no completadas</p>
                </div>
                <div class="icon">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">Mas info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-purple">
                <div class="inner">
                    <h3>6</h3>

                    <p>Incidencias Pendientes</p>
                </div>
                <div class="icon">
                    <i class="fa fa-question"></i>
                </div>
                <a href="#" class="small-box-footer">Mas info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>




    <div class="box">
        <div class="box-header with-border">
            <h1 class="box-title"><span class="fa fa-home"></span> Sectores controlados por ubicacion</h1>
        </div>
        <div class="box-body">
            <div class="row">



                <div class="col-md-6">
                    <div class="box-body table no-padding">
                        <h5><b>Sectores controlados por instancia hoy:</b></h5>
                        <table class="table table-hover">
                            <tbody><tr>
                                <th>Instancia</th>
                                <th>Sectores Controlados</th>

                            </tr>

                            @foreach($marks as $mark)
                                <tr>
                                    <td>{{$mark->instance_name}}</td>
                                    <td><span class="label label-success">{{$mark->marks_quantity}}</span></td>

                                </tr>
                            @endforeach



                            </tbody></table>
                    </div>
                </div>

                <div class="col-md-6">

                </div>

            </div>

        </div>


    </div>

@stop