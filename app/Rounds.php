<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rounds extends Model
{
    protected $fillable = ['name','hour_to_round','hour_to_round_finish','instance_id','enabled','enable_alert','mails_to_alert'];


    public function roundcards(){
        return $this->hasMany('App\Roundcards','round_id','id');
    }

    public function rounddays(){
        return $this->hasMany('App\Rounddays','round_id','id');
    }


    public function hasCard($card_id,$round_id){
        //verifica que sea un array, si es un array hace una busqueda del array

            return null !== $this->roundcards()->where('card_id', $card_id)->where('round_id',$round_id)->first();

    }



    public function hasDay($day_number,$round_id){
        //verifica que sea un array, si es un array hace una busqueda del array

        return null !== $this->rounddays()->where('day_number', $day_number)->where('round_id',$round_id)->where('enabled',1)->first();

    }

}
