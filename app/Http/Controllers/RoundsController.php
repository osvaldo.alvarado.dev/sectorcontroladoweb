<?php

namespace App\Http\Controllers;

use App\Rounds;
use Illuminate\Http\Request;
use App\Instances;
use App\Cards;
use App\Roundcards;
use App\Rounddays;
use Illuminate\Support\Facades\DB;


class RoundsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rounds  $rounds
     * @return \Illuminate\Http\Response
     */
    public function show(Rounds $rounds)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rounds  $rounds
     * @return \Illuminate\Http\Response
     */
    public function edit(Rounds $rounds)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rounds  $rounds
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rounds $rounds)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rounds  $rounds
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rounds $rounds)
    {
        //
    }


    public function add($instance_id){
        $instance = Instances::find($instance_id);
        $cards = Cards::all()->where('instance_id','=',$instance->id);
        $days = [1,2,3,4,5,6,7];
        $days_of_week=['','Domingo','Lunes','Martes','Miercoles','Jueves','viernes','Sabado'];

        return view('rounds.add',compact('instance','cards','days','days_of_week'));


    }


    public function addProcess($instance_id,Request $request){

        $input = $request->all();
        $instance = Instances::find($instance_id);

        //valida que vengan seleccionados las tarjetas
        if(!isset($input['cards'])){
            return back()->with('error', 'Por favor seleccione almenos una tarjeta')->withInput($input);
        }

        //valida que vengan seleccionados almenos 1 dia
        if(!isset($input['days'])){
            return back()->with('error', 'Por favor seleccione almenos un dia de ronda')->withInput($input);
        }

        $input['instance_id'] = $instance->id;
        $input['enabled'] = 1;

        //crea la ronda
        $round = Rounds::create($input);

        //define dias para iterarlos mas abajo
        $days = [1,2,3,4,5,6,7];
        $days_of_week=['','Domingo','Lunes','Martes','Miercoles','Jueves','viernes','Sabado'];

        if(isset($input['cards'])){
            foreach($input['cards'] as $card_id){
                $roundcard = new Roundcards();
                $roundcard->card_id = $card_id;
                $roundcard->round_id = $round->id;
                $roundcard->save();
            }
        }


        foreach($days as $day){
            $roundday = new Rounddays();
            $roundday->day_number = $day;
            $roundday->round_id = $round->id;
            $roundday->enabled = 0;

            //busca si el dia se paso como parametro para dejarlo habilitado de lo contrario
            //no lo habilita
            foreach($input['days'] as $day_selected){
                if($day == $day_selected){
                    $roundday->enabled = 1;
                }
            }

            $roundday->save();
        }



        $returnUrl = url('/')."/main/instances/".$instance->id;
        $sucess = true;
        return view('pages.genericprocess',compact('returnUrl','sucess'));

    }



    public function editround($round_id){
        $round = Rounds::find($round_id);
        $instance = Instances::find($round->instance_id);
        $cards = Cards::all()->where('instance_id','=',$instance->id);
        $days = [1,2,3,4,5,6,7];

        $days_of_week=['','Domingo','Lunes','Martes','Miercoles','Jueves','viernes','Sabado'];

        return view('rounds.add',compact('instance','cards','days','days_of_week','round'));


    }


    public function editroundprocess($round_id,Request $request){
        $input = $request->all();
        $round = Rounds::find($round_id);
        $instance = Instances::find($round->instance_id);

        //valida que vengan seleccionados las tarjetas
        if(!isset($input['cards'])){
            return back()->with('error', 'Por favor seleccione almenos una tarjeta')->withInput($input);
        }

        //valida que vengan seleccionados almenos 1 dia
        if(!isset($input['days'])){
            return back()->with('error', 'Por favor seleccione almenos un dia de ronda')->withInput($input);
        }





        //define dias para iterarlos mas abajo
        $days = [1,2,3,4,5,6,7];
        $days_of_week=['','Domingo','Lunes','Martes','Miercoles','Jueves','viernes','Sabado'];

        //borra todas las tarjetas y agrega las otras
        DB::table('roundcards')->where('round_id', $round->id)->delete();

        $round->update($input);

        if(isset($input['cards'])){
            foreach($input['cards'] as $card_id){
                $roundcard = new Roundcards();
                $roundcard->card_id = $card_id;
                $roundcard->round_id = $round->id;
                $roundcard->save();
            }
        }


        //updatea los dias y los deja todos deshabilitados para habilitarlos mas adelante
        DB::table('rounddays')
            ->where('round_id', $round->id)
            ->update(['enabled' => 0]);

        foreach($days as $day){
            $roundday = Rounddays::where('day_number','=',$day)->where('round_id','=',$round->id)->first();
            $roundday->day_number = $day;
            $roundday->round_id = $round->id;
            $roundday->enabled = 0;
            //busca si el dia se paso como parametro para dejarlo habilitado de lo contrario
            //no lo habilita
            foreach($input['days'] as $day_selected){
                if($day == $day_selected){
                    $roundday->enabled = 1;
                }
            }

            $roundday->save();
        }



        $returnUrl = url('/')."/main/instances/".$instance->id;
        $sucess = true;
        return view('pages.genericprocess',compact('returnUrl','sucess'));

    }
}
